// var mProgress;
// export default{
//     uploadProgress:mProgress
// }

const env = require('./config'); //配置文件，在这里配置你的OSS keyId和KeySecret,timeout:87600;
//更好的做法是把这些信息放到服务器进行签名，防止信息泄露

const Base64 = require('./Base64.js');

require('./hmac.js');
require('./sha1.js');
const Crypto = require('./crypto.js');

const dateFormat = function (time, fmt) {
    var o = {
        "M+": time.getMonth() + 1, // 月份
        "d+": time.getDate(), // 日
        "h+": time.getHours(), // 小时
        "m+": time.getMinutes(), // 分
        "s+": time.getSeconds(), // 秒
        "q+": Math.floor((time.getMonth() + 3) / 3), // 季度
        "S": time.getMilliseconds() // 毫秒
    };
    if (/(y+)/.test(fmt))
        fmt = fmt.replace(RegExp.$1, (time.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}
const geneRandomHexstr = function (length) {
    let text = ''
    let possible = '0123456789abcdef'
    for (let i = 0; i < length; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length))
    }
    return text
}

const getRandomFileName = function () {
    let fileName = dateFormat(new Date(), "yyyyMMddhhmmss") + geneRandomHexstr(4)
    return fileName
}

const uploadFile = function (filePath, dir, successc, failc) {
    // if (!filePath || filePath.length < 9) {
    //     wx.showModal({
    //         title: '图片错误',
    //         content: '请重试',
    //         showCancel: false,
    //     })
    //     return;
    // }

    console.log('上传图片…');
    const aliyunFileKey = dir + getRandomFileName();//我直接拿微信本地的名字当做传到服务器上的名字了，dir传的是images/，表示要传到这个目录下


    const aliyunServerURL = env.uploadImageUrl;//OSS地址，需要https
    const accessid = env.OSSAccessKeyId;

    const policyBase64 = getPolicyBase64();
    const signature = getSignature(policyBase64);//获取签名

    console.log('aliyunFileKey=', aliyunFileKey);
    console.log('aliyunServerURL', aliyunServerURL);
    let uploadTask = wx.uploadFile({
        url: aliyunServerURL,
        filePath: filePath,
        name: 'file',//必须填file
        formData: {
            'key': aliyunFileKey,
            'policy': policyBase64,
            'OSSAccessKeyId': accessid,
            'signature': signature,
            'success_action_status': '200',
        },
        success: function (res) {
            console.log('789');
            if (res.statusCode != 200) {
                failc(new Error('上传错误:' + JSON.stringify(res)))
                return;
            }
            console.log('上传图片成功', res)
            successc(aliyunFileKey);
        },
        fail: function (err) {
            err.wxaddinfo = aliyunServerURL;
            failc(err);
        },
    });
    // // 实时获取上传进度，图片文件太小，进度可能不是很明显
    uploadTask.onProgressUpdate((res) => {
        console.log("上传进度" + res.progress);
       // this.mProgress = res.progress;
    });
}

const getPolicyBase64 = function () {
    let date = new Date();
    date.setHours(date.getHours() + env.timeout);
    let srcT = date.toISOString();
    const policyText = {
        "expiration": srcT, //设置该Policy的失效时间，超过这个失效时间之后，就没有办法通过这个policy上传文件了 
        "conditions": [
            ["content-length-range", 0, 100 * 1024 * 1024] // 设置上传文件的大小限制,5mb
        ]
    };

    const policyBase64 = Base64.encode(JSON.stringify(policyText));
    return policyBase64;
}

const getSignature = function (policyBase64) {
    const accesskey = env.AccessKeySecret;

    const bytes = Crypto.HMAC(Crypto.SHA1, policyBase64, accesskey, {
        asBytes: true
    });
    const signature = Crypto.util.bytesToBase64(bytes);

    return signature;
}



module.exports = uploadFile;
