require('jbecool');
require('webmix');
const fs = require('fs');
const part1 = require('../../parts.js');



var pg1 = view({ class: " vbox cc" }).add(
    view({ class: "hbox cc", style: "w:700" }).add(
        view({ class: 'vbox mc', style: 'm:20' }).add(
            part1.mybgimg(102, 50, "./01.png").add(
                view({ class: 'hbox mc', style: "f:30;clr:#ffffff;", text: "病假" })
            ),
        ),
        view({ class: 'vbox mc', style: 'm:20' }).add(
            part1.mybgimg(176, 50, "./04.png").add(
                view({ class: 'hbox mc', style: "f:30;clr:#8d8d8d", text: "提醒事项" })
            ),
        ),
        view({ class: 'vbox mc', style: 'm:20' }).add(
            part1.mybgimg(130, 50, "./03.png").add(
                view({ class: 'hbox mc', style: "f:30;clr:#8d8d8d", text: "代接送" })
            ),
        ),
        view({ class: 'vbox mc', style: 'm:20' }).add(
            part1.mybgimg(102, 50, "./02.png").add(
                view({ class: 'hbox mc', style: "f:30;clr:#8d8d8d", text: "喂药" })
            ),
        ),
    ),

    a({
        style: "f:22;clr:#6e7ea2;mt:20",
        text: "查看历史嘱托"
    }),
    part1.mybottom("确定", "./09.png"),

    view({ class: "hbox cc", style: "w:700" }).add(
        image({ src: "./06.png", style: 'mr:auto;mt:20;mb:20' }),
        image({ src: "./08.png", style: "mt:20" }),

    ),

    view({ class: "hbox cc msb", style: "w:700;h:200;mt:20" }).add(
        textarea({ placeholder: '请输入嘱托内容，若疑似传染病请备注病症，以便学校知晓做好防控', style: 'bdr:0;h:200;w:700;f:28;clr:#868686' }),
        view({
            class: 'item hbox mc cc',
            style: 'f:22;clr:#8e8e8e;ml:auto',
            text: '0/20'
        }),
    ),
    view({ class: "hbox cc", style: "w:700" }).add(
        image({ src: "./10.png", style: "m:10" }),
    ),

)

fs.writeFileSync(__dirname + '\\yey-zt.vue', pg1.code, 'utf8');