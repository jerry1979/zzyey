require('jbecool');
require('webmix');
const fs = require('fs');

var w = 339,
    h = 211;
var pr = (750 - w * 2) / 3;

const part1 = require('../../parts.js');

var pg1 = view({ class: 'vbox cc' }).add(

    view({ class: 'hbox cc msb', style: "w:750;mt:30;mb:15" }).add(
        view({ class: 'hbox cc msb', style: "mt:30;mb:15" }).add(
            part1.mytext(30, "#000000", "本月"),
            image({ src: "./04.png" }),
        ),
        part1.mytext(24, "#8d8d8d8d", "3次嘱托"),
    ),


).add(
    view({ class: 'hbox msb', style: "w:750;h:250;mb:10;mt:20;bgimg:'./01.png'" }).add(
        view({ class: 'vbox mc' }).add(
            view({ class: 'hbox ', style: "ml:50" }).add(
                image({ src: '', style: "w:82;h:82" }),
                view({ class: 'vbox ' }).add(
                    part1.mytext(30, "#171717", "梁良子病假"),
                    part1.mytext(24, "#666666", "感冒生病请假"),

                )
            ),
            view({ class: 'hbox cc msb', style: 'w:650;mt:20;ml:40' }).add(
                part1.mytext(26, "#8d8d8d8d", "2020-0dd9-28"),
                part1.mybgimg(16, 24, "./03.png")
            ),
            image({ src: "./02.png", style: "w:750;h:1" }),
        ),


    ),
);

fs.writeFileSync(__dirname + '\\yey-ztlb.vue', pg1.code, 'utf8');