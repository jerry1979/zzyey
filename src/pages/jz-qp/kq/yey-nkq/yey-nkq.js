require('jbecool')
require('webmix')
const fs = require('fs')
const part1 = require('../../parts.js')

var part2 = part1.mybgimg(750, 38, './04.png')
part2._class = ['vbox cc']
var pg1 = template().add(
    view({ class: ' vbox cc ' }).add(
        view({
            class: 'hbox cc msb',
            style: 'w:789;h:583;mt:20;bgimg:./04.png',
        }).add(),
        view({ class: 'hbox cc msb', style: 'w:750;h:200;mt:20' }).add(
            view({ class: 'hbox cc c' }).add(
                view({ class: 'vbox cc', style: 'ml:20;mt:20' }).add(
                    image({ src: './06.png', style: 'mb:20;' }),
                    part1.mytext(22, '#5f5f5f', '坚持打卡的宝宝')
                ),
                view({ class: 'vbox cc', style: 'ml:20;mt:20' }).add(
                    image({ src: './08.png', style: 'mb:20;' }),
                    part1.mytext(22, '#5f5f5f', '坚持打卡的宝宝')
                ),
                view({ class: 'vbox cc', style: 'ml:20;mt:20' }).add(
                    image({ src: './07.png', style: 'mb:20;' }),
                    part1.mytext(22, '#5f5f5f', '坚持打卡的宝宝')
                )
            )
        ),
        image({ src: './05.png' })
    )
)

fs.writeFileSync(__dirname + '\\yey-nkq.vue', pg1.code, 'utf8')