require('jbecool');
require('webmix');
const fs = require('fs');
const part1 = require('../../parts.js');

var pg1 = template().add(view({ class: " vbox cc " }).add(
    view({ class: "vbox cc msa mc", style: { bgimg: "./16.png", w: 750, h: 400 } }).add(
        view({ class: "mc cc" }).add(
            view({ class: "hbox cc mc c", style: { bgimg: "./01.png", w: 267, h: 65, pl: 40 } }).add(
                image({ src: "./02.png" }),
                image({ src: "./03.png" }),
                part1.mytext(32, "#ffffff", "本月"),

            ),

            view({ class: "hbox cc msa", style: "w:750;mt:20" }).add(
                part1.mytext(28, "#ffffff", "考勤天数: 19天"),
                part1.mytext(28, "#ffffff", "未签到：4天"),
                image({ src: "./04.png" }),
            ),
            view({
                class: 'hbox  cc',
                style: { f: '25', w: '700', clr: '#ffffff', mt: '20', 'justify-content': 'flex-end' },
                text: '年度考勤'
            }),
        )
    ),
    view({ class: "hbox cc msb", style: "w:500;h:200;mt:20;mr:auto;ml:100" }).add(
        view({ class: "hbox cc" }).add(
            view({ class: "hbox cc", style: 'ml:20;mt:20' }).add(
                image({ src: "./-9.png" }),
                part1.mytext(20, "#3c3c3c", "黄色：请假"),
            ),
            view({ class: "hbox cc", style: 'ml:20;mt:20' }).add(
                image({ src: "-./12.png" }),
                part1.mytext(20, "#3c3c3c", "蓝色：当天已出园"),
            ),
            view({ class: "hbox cc", style: 'ml:20;mt:20' }).add(
                image({ src: "-./11.png" }),
                part1.mytext(20, "#3c3c3c", "红色：缺勤"),
            ),
            view({ class: "hbox cc", style: 'ml:20;mt:20' }).add(
                image({ src: "-./10.png" }),
                part1.mytext(20, "#3c3c3c", "绿色：签到"),
            ),




        )
    ),
    view({ class: "hbox cc msb", style: "w:750;h:500;mt:20" }).add(),
))

fs.writeFileSync(__dirname + '\\yey-ykq.vue', pg1.code, 'utf8');