exports.root = "https://gitee.com/jerry1979/zzyey/raw/master/src/pages";

exports.fixUrl = function (dirname, url) {
    var idx = dirname.lastIndexOf("\\pages\\") + 7;
    var r = J.us.replaceAll(dirname.substr(idx), "\\", "/");
    return "https://gitee.com/jerry1979/zzyey/raw/master/src/pages/" + r + "/" + url;
};

exports.mybottom = function (str, img) {
    return button({
        style: {
            'outline': '0 none !important',
            'border-radius': '26px',
            mt: '10%',
            mb: '10%',
            bgimg: img,
            w: 573,
            h: 71,
            f: 32,
            clr: "#ffffff"
        },
        class: "c",
        text: str
    })
};

exports.myinput = function (h, f, clr, str) {
    return input({
        style: {
            bdr: 'none',
            'outline': 'none',
            h: h,
            f: f,
            clr: clr
        },
        placeholder: str
    })
};

exports.mybgimg = function (w, h, img) {
    return view({
        style: {
            w: w,
            h: h,
            bgimg: img
        }
    })
};

exports.mytext = function (f, clr, str) {
    return view({
        style: {
            ml: 15,
            mr: 15,
            f: f,
            clr: clr
        },
        text: str
    })
};