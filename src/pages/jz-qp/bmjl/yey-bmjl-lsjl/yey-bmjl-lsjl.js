require('jbecool');
require('webmix');
const fs = require('fs');

var w = 339,
    h = 211;
var pr = (750 - w * 2) / 3;

const part1 = require('../../parts.js');

var pg1 = view({ class: 'vbox cc' }).add(

    view({ class: 'hbox msb cc', style: "w:750;mt:30;mb:15" }).add(
        view({ class: 'hbox cc msb', style: "mt:30;mb:15" }).add(
            part1.mytext(30, "#000000", "本月"),
            image({ src: "./05.png" }),
        ),
        image({ src: "./04.png", style: "w:750" }),
    ),
).add(
    view({ class: 'hbox cc msa', style: "w:750;h:200;mb:10;mt:20;bgimg:./02.png" }).add(
        view({ class: 'hbox', style: 'ml:50' }).add(
            image({ src: './06.png', style: "w:82;h:82" }),
            view({ class: 'vbox msa' }).add(
                part1.mytext(30, "#343434", "周末活动"),
                part1.mytext(24, "#8d8d8d8d", "赵老师 2020.09.28 22:17"),

            )
        ),
        view({ class: 'vbox cc msa', style: "mr:50" }).add(
            part1.mybgimg(127, 50, "./01.png").add(
                view({ class: 'hbox mc', style: "f:30;clr:#ffffff", text: "已参与" })
            ),
        )
    ),
);

fs.writeFileSync(__dirname + '\\yey-bmjl-lsjl.vue', pg1.code, 'utf8');