require('jbecool');
require('webmix');
const fs = require('fs');

var w = 339,
    h = 211;
var pr = (750 - w * 2) / 3;

const part1 = require('../../parts.js');


var pg1 = view({ class: 'vbox cc' }).add(

    view({ class: 'hbox cc msb', style: "w:750;mt:30;mb:15" }).add(
        view({ class: 'hbox cc msb', style: "mt:30;mb:15" }).add(
            part1.mytext(30, "#000000", "本月"),
            image({ src: "./07.png" }),
        ),
        part1.mytext(24, "#465584", "历史记录"),
    ),
    image({ src: "./1.png", style: "w:750;mb:20" }),


).add(
    part1.mybgimg(173, 44, "./02.png").add(
        view({ class: 'hbox mc', style: "f:26;clr:#ffffff", text: "2020-09-28" })
    ),
    view({ class: 'msa hbox', style: "w:750;h:250;mb:10;mt:20;bgimg:'./03.png'" }).add(
        view({ class: 'hbox cc' }).add(
            image({ src: './06.png', style: "w:82;h:82" }),
            view({ class: 'vbox' }).add(
                part1.mytext(30, "#343434", "周末活动"),
                part1.mytext(26, "#66666666", "已参与:20人"),
                part1.mytext(24, "#8d8d8d8d", "赵老师 2020.09.28 22:17"),
            )
        ),

        view({ class: 'vbox mc' }).add(
            part1.mybgimg(100, 50, "./05.png").add(
                view({ class: 'hbox mc', style: "f:30;clr:#ffffff", text: "参与" })
            ),
        )
    ),
);

fs.writeFileSync(__dirname + '\\yey-bmjl-lb.vue', pg1.code, 'utf8');