require('jbecool');
require('webmix');
const fs = require('fs');

var w = 339,
    h = 211;
var pr = (750 - w * 2) / 3;

const part1 = require('../../parts.js');

var pg1 = view({ class: 'vbox cc' }).add(
    view({ class: 'vbox mc', style: "w:750;h:100;mb:10;mt:20'" }).add(
        view({ class: 'hbox msb cc', style: "w:750;mt:20" }).add(
            part1.mytext(30, "#000000", "周末活动"),
            part1.mytext(22, "#8e8e8e", "2020-09-25"),
        ),

    ),
    view({ class: 'vbox', style: "mt:30", }).add(
        part1.mybottom("确认报名", "./06.png"),
    ),
    view({ class: "hbox cc msb", style: "w:789;h:583;mt:20;" }).add(),
    view({ class: 'vbox mc', style: "w:750;h:250;mb:10;mt:50" }).add(
        part1.mytext(30, "#000000", "敬老院看望老人。"),
        part1.mytext(20, "#8e8e8e", "截止时间：2020-09-28 23:59:00"),
        image({ src: './02.png', style: "w:326;h:197;m:10" }),
    ),
    image({ src: './01.png' }),
);

fs.writeFileSync(__dirname + '\\yey-bmjl-cyjl.vue', pg1.code, 'utf8');