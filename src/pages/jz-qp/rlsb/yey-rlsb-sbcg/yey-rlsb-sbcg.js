require('jbecool');
require('webmix');
const fs = require('fs');
const part1 = require('../../parts.js');
var w = 339,
    h = 211;
var pr = (750 - w * 2) / 3;

var pg1 = view({ class: 'vbox ', style: { p: pr } }).add(

    view({ class: 'c' }).add(
        image({
            src: './01.png',
            style: "w:430;h:430;m:20"
        }),
    ),
    view({ class: 'vbox', style: "mt:30", }).add(
        part1.mybottom("点击进入", "./02.png"),
    ),
    view({
        class: 'c',
        style: "f:45;clr:#6d9b85;mt:80",
        text: "采集成功"
    }),

);

fs.writeFileSync(__dirname + '\\yey-rlsb-sbcg.vue', pg1.code, 'utf8');