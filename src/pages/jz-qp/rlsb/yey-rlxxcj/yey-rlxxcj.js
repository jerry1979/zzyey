require('jbecool');
require('webmix');
const fs = require('fs');
const part1 = require('../../parts.js');
var w = 339,
    h = 211;
var pr = (750 - w * 2) / 3;

var pg1 = view({ class: 'vbox cc', style: { p: pr } }).add(
    view({}).add(
        image({
            src: './03.png',
            style: "w:291;h:291;m:20"
        }),
        view({

            style: "f:22;clr:#e45858;mt:20",
            text: "提示：您还未采集宝宝考勤人脸信息",
        }),
    ),


    view({}).add(
        image({
            src: './05.png',
            style: "w:750;h:163;m:20"
        })
    ),
    view({
        style: "f:32;clr:#585858;w:700",
        text: "2、请将脸置于提示框内，并按提示完成动作。",
    }),
    view({
        style: "f:32;clr:#585858;mt:30;w:700",
        text: "1、请确认由本人亲自操作；",
    }),

    image({ src: "./04.png", style: "mt:30" }),


).add(
    view({ class: 'vbox', }).add(
        part1.mybottom("开始检测", "./06.png"),
    ),
);

fs.writeFileSync(__dirname + '\\yey-rlxxcj.vue', pg1.code, 'utf8');