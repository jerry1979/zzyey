require('jbecool');
require('webmix');
const fs = require('fs');

var w = 339, h = 211;
var pr = (750 - w * 2) / 3;

const part1 = require('../../parts.js');
var pg1 = view({ class: 'vbox cc' }).add(
    view({ class: 'vbox mc', style: "w:750;h:250;mb:10;mt:20;bgimg:'./01.png'" }).add(
        view({ class: 'hbox msb cc', style: "ml:50;w:650;mt:20" }).add(
            part1.mytext(30, "#171717", "国庆节放假通知"),
            image({ src: "./02.png", style: "" }),
            part1.mytext(22, "#666666", "2020-0dd9-28"),
        ),
        view({ class: 'hbox cc', style: 'mt:20;ml:40;h:100;mr:20' }).add(
            part1.mytext(26, "#8d8d8d8d", "【国庆节放假通知】10月1日-10月8日放假八天，期间有任何事项可以通过微信或者电话直接与老师联系。"),
        )
    )
);

var ss = script().add(function () {

    export default {
        data() {
          return {
            xxsDate:{},
            
            }
        },
        onLoad() {
            this.getxxsDates();
        },
        //此处为自定义方法
        methods: {
           getxxsDates(){
               var _self = this;
               uni.request({
                   url: 'http://47.106.144.18/smartkids/note/notedetail', //请求接口
                   data: {
                        curPage:1,
                        pageSize:1,
                        oSiId:1,
                        oCiId:1,
                    },
                   method:'POST',
                   header: {
                         'content-type': 'application/x-www-form-urlencoded',  //自定义请求头信息
                       },
                success: (res) => {
                    console.log(res);
                    res.data.data.rows.forEach(element => {
                        element.headPortrait = "http://47.106.144.18/smartkids/" + element.headPortrait;
                        for(var i=0;i<element.bmpPath.length;i++){
                            element.bmpPath[i] = "http://47.106.144.18/smartkids/" + element.bmpPath[i]
                        }
                    });
                       _self.xxsDate=res.data.data;
                   },
                   fail:(msg) =>{
                       console.log(msg);
                   }
                });
            }
        }
    }

});

fs.writeFileSync(__dirname + '\\yey-tz.vue', pg1.code, 'utf8');
