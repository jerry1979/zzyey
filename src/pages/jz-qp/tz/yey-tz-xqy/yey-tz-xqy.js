require('jbecool');
require('webmix');
const fs = require('fs');

var w = 339, h = 211;
var pr = (750 - w * 2) / 3;

const part1 = require('../../parts.js');

var pg1 = view({ class: 'vbox cc' }).add(
    view({ class: 'vbox cc', style: 'bgimg:./01.png;h:650;w:710' }).add(
        view({ class: 'hbox msb', style: "w:710;mb:10;mt:100" }).add(
            view({ class: 'hbox' }).add(
                image({ src: './05.png', style: "w:82;h:82" }),
                view({ class: 'vbox' }).add(
                    part1.mytext(30, "#f18e4b", "公告来啦！"),
                    part1.mytext(22, "#8d8d8d", "2020-9-28 13:00"),
                )
            )
        ),
        view({ class: 'hbox', style: "mr:auto ;ml:30" }).add(
            image({ src: '', style: "w:139;h:139;m:10" }),
            image({ src: '', style: "w:139;h:139;m:10" }),
        ),
        view({ class: 'vbox mc', style: "w:750;mb:10;mt:20;" }).add(
            view({ class: 'hbox msb cc', style: "ml:50;w:650;" }).add(
                part1.mytext(30, "#171717", "国庆节放假通知"),
            ),
            view({ class: 'hbox cc', style: 'mt:20;ml:40;h:100;mr:20' }).add(
                part1.mytext(26, "#8d8d8d8d", "【国庆节放假通知】10月1日-10月8日放假八天，期间有任何事项可以通过微信或者电话直接与老师联系。"),
            ),

        ),
        image({ src: "./03.png", style: "mt:30" }),
    )


);

fs.writeFileSync(__dirname + '\\yey-tz-xqy.vue',  pg1.code, 'utf8');
