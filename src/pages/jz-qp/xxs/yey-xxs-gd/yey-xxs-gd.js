require('jbecool');
require('webmix');
const fs = require('fs');

var w = 339,
    h = 211;
var pr = (750 - w * 2) / 3;

const part1 = require('../../parts.js');


var pg1 = template().add(view({
        class: 'vbox cc'
    }).add(

        view({
            class: 'cc',
            style: "m:20"
        }).add(
            view({
                class: 'hbox msb',
                style: "w:750;mb:10"
            }).add(
                view({
                    class: 'hbox'
                }).add(

                    image({
                        src: './02.png',
                        style: "w:82;h:82"
                    }),
                    view({
                        class: 'vbox'
                    }).add(
                        part1.mytext(30, "#171717", "和平公园游玩"),
                        part1.mytext(24, "#8d8d8d", "9-19 10:36"),
                    )
                )
            ),
            view({
                class: 'cc'
            }).add(
                view({
                    class: 'hbox',
                    style: 'w:750;bgclr:#e6edf4;mt:20'
                }).add(
                    image({
                        src: './05.png',
                        style: "w:40;h:40;m:10"
                    }),
                    image({
                        src: './06.png',
                        style: "w:40;h:40;m:10"
                    }),
                    image({
                        src: './06.png',
                        style: "w:40;h:40;m:10"
                    }),
                    image({
                        src: './06.png',
                        style: "w:40;h:40;m:10"
                    }),
                ),
                view({
                    class: 'hbox cc',
                    style: "bgclr:#e6edf4;h:50"
                }).add(
                    part1.mytext(24, "#6c87be", "梁子妈妈："),
                    part1.mytext(24, "#585858", "去和平公园游玩啦？"),
                ),
                view({
                    class: 'hbox cc',
                    style: "bgclr:#e6edf4;h:50"
                }).add(
                    part1.mytext(24, "#6c87be", "梁子妈妈："),
                    part1.mytext(24, "#585858", "去和平公园游玩啦？"),
                ),
            ),
            view({
                class: 'hbox tl',
                style: "justify-content: flex-end"
            }).add(
                image({
                    src: './04.png',
                    style: "w:32;h:33;"
                }),

                part1.mytext(22, "#535355", "分享"),

                image({
                    src: './03.png',
                    style: "w:32;h:33;"
                }),

                part1.mytext(22, "#535355", "10"),

            ),
            view({
                class: 'hbox',
                style: 'w:350'
            }).add(
                image({
                    src: '',
                    style: "w:139;h:139;m:10"
                }),
                image({
                    src: '',
                    style: "w:139;h:139;m:10"
                }),
                image({
                    src: '',
                    style: "w:139;h:139;m:10"
                }),
                image({
                    src: '',
                    style: "w:139;h:139;m:10"
                }),
            ),
            part1.mytext(28, "#171717", "和平公园游玩啦!! 大家都在这里等你哦~"),
        ),
    )

);

fs.writeFileSync(__dirname + '\\yey-xxs-gd.vue', pg1.code, 'utf8');