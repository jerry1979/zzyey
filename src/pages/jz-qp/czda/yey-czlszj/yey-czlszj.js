require('jbecool');
require('webmix');
const fs = require('fs');

var w = 339,
    h = 211;
var pr = (750 - w * 2) / 3;

const part1 = require('../../parts.js');

var pg1 = view({ class: 'vbox cc' }).add().add(
    view({ class: 'hbox msb', style: "w:750;h:280;mb:10;mt:20;bgimg:'./01.png'" }).add(
        view({ class: 'vbox mc' }).add(
            view({ class: 'hbox cc msb', style: 'w:650;ml:20' }).add(
                part1.mytext(28, '#171717', '2020春季 第一周')
            ),
            view({ class: 'hbox ', style: "ml:40" }).add(
                view({ class: 'hbox ', style: 'w:160' }).add(
                    image({ src: '', style: "w:65;h:65;mr:5;mt:5" }),
                    image({ src: '', style: "w:65;h:65;mr:5;mt:5" }),
                    image({ src: '', style: "w:65;h:65;mr:5;mt:5" }),
                    image({ src: '', style: "w:65;h:65;mr:5;mt:5" }),
                ),
                view({ class: 'vbox  msa ' }).add(
                    view({ class: 'hbox msa', style: 'w:400' }).add(
                        view({
                            class: 'item hbox mc cc',
                            style: { f: 28, clr: "#171717" },
                            text: '生活表现'
                        }),
                        image({ src: "./02.png", style: "" }),
                    ),
                    view({ class: 'hbox msa', style: 'w:400' }).add(
                        view({
                            class: 'item hbox mc cc',
                            style: { f: 28, clr: "#171717" },
                            text: '学习表现'
                        }),
                        image({ src: "./02.png", style: "" }),
                    ),
                )
            ),

        ),


    )
);

fs.writeFileSync(__dirname + '\\yey-czlszj.vue', pg1.code, 'utf8');