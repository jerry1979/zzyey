require('jbecool');
require('webmix');
const fs = require('fs');

var w = 339,
    h = 211;
var pr = (750 - w * 2) / 3;

const part1 = require('../../parts.js');

var pg1 = view({ class: 'vbox cc' }).add(
    view({ class: 'vbox cc' }).add(
        view({ class: 'hbox msb cc', style: "w:710;mb:10;mt:20" }).add(
            part1.mytext(30, "#171717", "2020年春季第一周"),
            image({ src: "./03.png", style: "w:16;h:24" }),
        ),
        part1.mybottom("确定", "./04.png"),
        view({ class: "hbox cc msb", style: "w:700;h:200;mt:20" }).add(
            textarea({ placeholder: '请输入留言', style: 'bdr:0;h:200;w:700;f:28;clr:#868686' }),
            view({
                class: 'item hbox mc cc',
                style: 'f:22;clr:#8e8e8e;ml:auto',
                text: '0/50'
            }),
        ),
        image({ src: "./01.png", style: "mt:10" }),
        view({ class: 'hbox msb', style: "w:710;mb:10;mt:20" }).add(
            part1.mytext(30, "#171717", "家长留言"),
        ),
        image({ src: './02.png', style: "mt:20;w:700" }),
        view({ class: 'vbox mc', style: "w:750;mb:10;mt:20;" }).add(
            view({ class: 'hbox msb cc', style: "ml:20;w:650;" }).add(
                part1.mytext(28, "#171717", "本周，周珊珊学校表现不错。本周，周珊珊学校表现不错。本周，周珊珊学校表现不错。"),
            ),
        ),
        image({ src: "./01.png", style: "mt:10" }),
        view({ class: 'hbox msb', style: "w:710;mb:10;mt:20" }).add(
            part1.mytext(30, "#171717", "老师评语"),
        ),

        image({ src: './02.png', style: "mt:20;w:700" }),
        view({ class: 'vbox ', style: 'w:700' }).add(
            view({ class: 'hbox msb', style: 'w:400;mt:20' }).add(
                view({
                    class: 'item hbox mc cc',
                    style: { f: 30, clr: "#171717" },
                    text: '生活表现'
                }),
                view({ class: 'hbox' }).add(
                    image({ src: "./06.png", style: "" }),
                    image({ src: "./06.png", style: "" }),
                    image({ src: "./06.png", style: "" }),
                )
            ),
            view({ class: 'hbox msb', style: 'w:400;mt:10' }).add(
                view({
                    class: 'item hbox mc cc',
                    style: { f: 30, clr: "#171717" },
                    text: '学习表现'
                }),
                view({ class: 'hbox' }).add(
                    image({ src: "./06.png", style: "" }),
                    image({ src: "./06.png", style: "" }),
                    image({ src: "./06.png", style: "" }),
                )
            ),
            image({ src: "./01.png", style: "mt:10" }),
        ),
        image({ src: './02.png', style: "mt:20;w:700" }),
        view({ class: 'hbox', style: "mr:auto ;ml:30" }).add(
            image({ src: '', style: "w:139;h:139;m:10" }),
        ),
        view({ class: 'vbox mc', style: "w:750;mb:10;mt:20;" }).add(
            view({ class: 'hbox msb cc', style: "ml:20;w:650;" }).add(
                part1.mytext(30, "#171717", "精彩瞬间"),
            ),
        ),
        image({ src: "./01.png", style: "" }),
    )


);

fs.writeFileSync(__dirname + '\\yey-czzj.vue', pg1.code, 'utf8');