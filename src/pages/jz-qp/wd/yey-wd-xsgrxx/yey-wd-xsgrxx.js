require('jbecool');
require('webmix');
const fs = require('fs');

var w = 339, h = 211;
var pr = (750 - w * 2) / 3;

const part1 = require('../../../parts.js');

var ss = style().link('/node_modules/jbecool/becool.min.css');

var pg1 = view({ class: 'vbox cc', style: 'mt:20' }).add(
    view({ class: 'hbox cc msb', style: "w:750;mb:10" }).add(
        part1.mybgimg(75, 75, '01.png'),
        view({ class: 'hbox cc', }).add(
            part1.mytext(26, "#8d8d8d8d", "点击上传头像"),
            part1.mybgimg(16, 25, '03.png')
        )
    ),
    part1.mybgimg(750, 25, '05.png'),
    view({ class: 'hbox cc msb', style: "w:750;mb:10;h:60" }).add(
        part1.mytext(30, "#171717", "家长人脸信息采集"),
        view({ class: 'hbox cc', }).add(
            part1.mybgimg(16, 25, '03.png')
        )
    ),
    part1.mybgimg(750, 25, '05.png'),
    view({ class: 'hbox cc msb', style: "w:750;mb:10;h:60" }).add(
        part1.mytext(30, "#171717", "电话号码"),
        view({ class: 'hbox cc', }).add(
            part1.myinput(30, 30, '#8d8d8d8d', '自动获取'),
            part1.mybgimg(16, 25, '03.png')
        )
    ),
    image({ src: "02.png", style: "w:720;h:1" }),
    view({ class: 'hbox cc msb', style: "w:750;mb:10;h:60" }).add(
        part1.mytext(30, "#171717", "亲属关系"),
        view({ class: 'hbox cc', }).add(
            part1.myinput(30, 30, '#8d8d8d8d', '妈妈'),
            part1.mybgimg(16, 25, '03.png')
        )
    ),
    image({ src: "02.png", style: "w:720;h:1" }),
    view({ class: 'hbox cc msb', style: "w:750;mb:10;h:60" }).add(
        part1.mytext(30, "#171717", "家长姓名"),
        view({ class: 'hbox cc', }).add(
            part1.myinput(30, 30, '#8d8d8d8d', '请输入家长姓名'),
            part1.mybgimg(16, 25, '03.png')
        )
    ),
    image({ src: "02.png", style: "w:720;h:1" }),
    view({ class: 'hbox cc msb', style: "w:750;mb:10;h:60" }).add(
        part1.mytext(30, "#171717", "学生身份证号"),
        view({ class: 'hbox cc', }).add(
            part1.myinput(30, 30, '#8d8d8d8d', '请输入学生身份证号'),
            part1.mybgimg(16, 25, '03.png')
        )
    ),
    image({ src: "02.png", style: "w:720;h:1" }),
    view({ class: 'hbox cc msb', style: "w:750;mb:10;h:60" }).add(
        part1.mytext(30, "#171717", "学生姓名"),
        view({ class: 'hbox cc', }).add(
            part1.myinput(30, 30, '#8d8d8d8d', '顾子子'),
            part1.mybgimg(16, 25, '03.png')
        )
    ),
    image({ src: "02.png", style: "w:720;h:1" }),

).add(
    view({ class: 'hbox mc', style: "w:750;mt:40;align-items:flex-end" }).add(
        button({
            style: {
                'outline': '0 none !important',
                'border-radius': '26px',
                bgimg: "06.png",
                w: 381,
                h: 54,
                f: 34,
                clr: "#ffffff"
            },
            text: "确定"
        })
    ),
);

fs.writeFileSync(__dirname + '\\yey-wd-xsgrxx.html', ss.code + pg1.code, 'utf8');
