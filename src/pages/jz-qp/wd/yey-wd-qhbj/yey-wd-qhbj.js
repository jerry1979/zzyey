require('jbecool');
require('webmix');
const fs = require('fs');

var w = 339, h = 211;
var pr = (750 - w * 2) / 3;

const part1 = require('../../../parts.js');
var ss = style().link('/node_modules/jbecool/becool.min.css');

var pg1 = view({ class: 'vbox cc' }).add(

    view({ class: 'hbox cc msb', style: "w:750;mt:30" }).add(
        part1.mytext(),
        part1.mytext(22, "#858589", "您已加入以下班级"),
    ),


).add(
    view({ class: 'hbox msb', style: "w:750;h:300;mb:10;mt:20;bgimg:'03.png'" }).add(
        view({ class: 'vbox mc', style: 'w:700' }).add(
            view({ class: 'hbox msa', style: "ml:50" }).add(
                view({ class: 'hbox ', style: 'w:160' }).add(
                    image({ src: '05.png', style: "w:72;h:72;mr:5;mt:5" }),
                    image({ src: '05.png', style: "w:72;h:72;mr:5;mt:5" }),
                    image({ src: '05.png', style: "w:72;h:72;mr:5;mt:5" }),
                    image({ src: '05.png', style: "w:72;h:72;mr:5;mt:5" }),
                ),
                part1.mybgimg(33, 33, "01.png"),
                view({ class: 'vbox ' }).add(
                    part1.mytext(30, "#343434", "大班 班级名称"),
                    part1.mytext(24, "#666666", "包子 妈妈 梁良子 爸爸"),
                ),
            ),

        ),


    ),
    image({ src: "02.png", style: "w:750;h:1" }),
).add(
    view({ class: 'vbox', }).add(
        part1.mybottom("加入此班级", "06.png"),
    ),
);

fs.writeFileSync(__dirname + '\\yey-wd-qhbj.html', ss.code + pg1.code, 'utf8');
