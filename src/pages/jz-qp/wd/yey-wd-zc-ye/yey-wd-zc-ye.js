require('jbecool');
require('webmix');
const fs = require('fs');

var w = 339, h = 211;
var pr = (750 - w * 2) / 3;

const part1 = require('../../parts.js');


var pg1 = view({ class: 'vbox cc', style: { p: pr } }).add(

    image({
        src: '02.png',
        style: "w:123;h:123;"
    }),

    view({ class: 'vbox' }).add(

        button({
            class: 'c',
            style: "w:402;h:71;bgimg:01.png;clr:ffffff;f:32;bdr:none;bgclr:#ffffff;mt:100",
            text: "退费"
        }),
        view({
            class: 'u c',
            style: "f:22;clr:#3e896b;mt:30;",
            text: "学费退费说明"
        }),
    ),

    view({ class: 'hbox cc', style: "mt:50" }).add(
        view({
            style: "f:68;clr:#000000;",
            text: "￥502.9"
        })
    ),
    view({ class: "hbox cc mc ", style: "w:750;h:50;mt:30" }).add(
        part1.mytext(34, "#000000", "学费"),
        image({ src: "03.png", style: "w:24;h:16;mr:20" }),
    ),

).add(

);

fs.writeFileSync(__dirname + '\\yey-wd-zc-ye.vue',pg1.code, 'utf8');
