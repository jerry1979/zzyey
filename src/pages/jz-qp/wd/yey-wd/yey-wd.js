require('jbecool');
require('webmix');
const fs = require('fs');
const part1 = require('../../parts.js');
var pg1 = view({ class: " vbox cc" }).add(
    view({ class: "hbox cc", style: { bgimg: "./11.png", w: 750, h: 318 } }).add(
        image({ src: "./01.png", style: "w:125;h:125;m:20" }),
        view({ class: "hbox msb", style: "w:550;h:125;" }).add(
            view({ class: "vbox mc" }).add(
                part1.mytext(30, "#ffffff", "李抒樾 XXX家长"),
                view({ class: "hbox mc" }).add(
                    view({ class: "hbox cc", style: "" }).add(
                        part1.mytext(24, "#ffffff", "(已加入1个班)"),
                    ),
                    view({ class: "hbox cc", style: "" }).add(
                        image({ src: "./02.png", style: "w:25;h:25;ml:15" }),
                        part1.mytext(24, "#ffffff", "切换班级"),
                    )
                ),

            ),
            view({ class: "vbox msb", style: "align-items:flex-end" }).add(
                part1.mybgimg(121, 35, "./10.png").add(
                    view({ class: 'hbox mc', style: "f:24;clr:#ffffff", text: "学生信息" })
                )

            )
        )
    ),

    image({ src: "./06.png", style: "w:750;h:18" }),


    view({ class: "hbox cc msb", style: "w:700;h:80" }).add(
        view({ class: "hbox cc" }).add(
            image({ src: "./05.png", style: "w:25;h:25;m:10" }),
            part1.mytext(32, "#171717", "清除缓存")
        ),
        view({ class: "hbox cc" }).add(
            part1.mytext(20, "#878787", "13M"),
            image({ src: "./09.png", style: "w:16;h:24" }),
        ),
    ),


    image({ src: "./10.png", style: "w:715;h:1" }),

    view({ class: "hbox cc msb", style: "w:700;h:80" }).add(
        view({ class: "hbox cc" }).add(
            image({ src: "./04.png", style: "w:25;h:25;m:10" }),
            part1.mytext(32, "#171717", "客服/帮助")
        ),
        image({ src: "./09.png", style: "w:16;h:24" }),
    ),

    image({ src: "./06.png", style: "w:750;h:18" }),
    image({ src: "./10.png", style: "w:715;h:1" }),

    view({ class: "hbox cc msb", style: "w:700;h:80" }).add(
        view({ class: "hbox cc" }).add(
            image({ src: "./03.png", style: "w:25;h:25;m:10" }),
            part1.mytext(32, "#171717", "加入新班级")
        ),
        image({ src: "./09.png", style: "w:16;h:24" }),
    ),

).add(
    view({ class: 'vbox', }).add(
        part1.mybottom("退出此班级", "./08.png"),
    ),
);

fs.writeFileSync(__dirname + '\\yey-wd.vue', pg1.code, 'utf8');