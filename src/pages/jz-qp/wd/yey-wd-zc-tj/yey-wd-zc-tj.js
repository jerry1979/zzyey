require('jbecool');
require('webmix');
const fs = require('fs');
const part1 = require('../../parts.js');
var pg1 = view({ class: " vbox cc " }).add(
    view({ class: "vbox cc", style: "w:750;h:300;bgimg:02.png" }).add(
        view({ class: "hbox cc msa", style: "w:710;mt:50" }).add(
            view({ class: "vbox ", style: 'ml:20;mt:20' }).add(
                part1.mytext(36, "#ffffff", "学期账单"),
                image({ src: "01.png", style: 'mb:20;' }),
            ),
            part1.mytext(36, "#ffffff", "年账单"),
        ),
    ),

    view({ class: "msb", style: "w:750;h:632;bgimg:09.png;mt:-150" }).add(
        view({ class: "hbox msa", style: "w:750;h:50;mt:100" }).add(
            view({ class: "hbox ", style: 'w:165;h:50;bgimg:03.png' }).add(
                view({
                    style: { m: '0 auto', f: 30, clr: '#39ae8e' },
                    text: '全部'
                }),
            ),
            view({ class: "hbox ", style: 'w:165;h:50;bgimg:04.png' }).add(
                view({
                    style: { m: '0 auto', f: 30, clr: '#ffffff' },
                    text: '学杂费'
                }),
            ),
            view({ class: "hbox ", style: 'w:165;h:50;bgimg:04.png' }).add(
                view({
                    style: { m: '0 auto', f: 30, clr: '#ffffff' },
                    text: '伙食费'
                }),
            ),
            view({ class: "hbox ", style: 'w:165;h:50;bgimg:04.png' }).add(
                view({
                    style: { m: '0 auto', f: 30, clr: '#ffffff' },
                    text: '学费'
                }),
            ),
        ),
        view({ class: 'hbox cc' }).add(
            view({
                style: "f:56;clr:#39ae8e;mt:20",
                text: "￥6002.20"
            })
        ),
        view({
            style: "f:24;clr:#57b79b;mt:20;ml:10",
            text: "共支出45笔"
        }),
        view({ class: "hbox cc ", style: "w:750;h:50;bgimg:'05.png';mt:20" }).add(
            part1.mytext(32, "#171717", "2020年12月"),
            image({ src: "06.png", style: "w:24;h:16;mr:20" }),
        ),
        view({ class: "hbox", style: "w:750;h:50;mt:20;ml:20" }).add(
            view({ class: "hbox ", style: 'w:118;h:48;bgimg:07.png' }).add(
                view({
                    style: { m: '0 auto', f: 30, clr: '#39ae8e' },
                    text: '缴费'
                }),
            ),
            view({ class: "hbox ", style: 'w:118;h:48;' }).add(
                view({
                    style: { m: '0 auto', f: 30, clr: '#535353' },
                    text: '扣费'
                }),
            )
        ),
    ),
)

fs.writeFileSync(__dirname + '\\yey-wd-zc-tj.vue',pg1.code, 'utf8');