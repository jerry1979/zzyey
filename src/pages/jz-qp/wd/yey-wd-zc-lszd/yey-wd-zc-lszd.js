require('jbecool');
require('webmix');
const fs = require('fs');
const part1 = require('../../parts.js');


var pg1 = view({ class: " vbox cc " }).add(
    view({ class: "hbox cc msb", style: "w:750;h:80;bgimg:'01.png'" }).add(
        part1.mytext(30, "#171717", "全部类型"),
        image({ src: "02.png", style: "w:16;h:24;mr:20" }),
    ),
    view({ class: "hbox cc msb", style: "w:750;h:100;" }).add(
        view({ class: "hbox cc" }).add(
            image({ src: "05.png" }),
            view({ class: "vbox" }).add(
                part1.mytext(28, "#171717", "缴费 学费"),
                part1.mytext(22, "#acacac", "2月9日09:46"),
            )
        ),
        part1.mytext(28, "#171717", "+3800.85")
    ),

    image({ src: "06.png" }),
    view({ class: "hbox cc msb", style: "w:750;h:100;" }).add(
        view({ class: "hbox cc" }).add(
            image({ src: "04.png" }),
            view({ class: "vbox" }).add(
                part1.mytext(28, "#171717", "扣款 学费"),
                part1.mytext(22, "#acacac", "9月1日09:46"),
            )
        ),
        part1.mytext(28, "#efb025", "-500.85")
    ),

    image({ src: "03.png" }),
    view({ class: "hbox cc msb", style: "w:750;h:80;" }).add(
        part1.mytext(30, "#171717", "2020年 春季"),
        image({ src: "02.png", style: "w:16;h:24;mr:20" }),
    ),



)

fs.writeFileSync(__dirname + '\\yey-wd-zc-lszd.vue', pg1.code, 'utf8');