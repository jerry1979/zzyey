require('jbecool');
require('webmix');
const fs = require('fs');

var w = 339,
    h = 211;
var pr = (750 - w * 2) / 3;

const part1 = require('../../parts.js');

var pg1 = view({ class: 'vbox cc', style: { p: pr } }).add(

    image({
        src: './02.png',
        style: "w:123;h:123;"
    }),

    view({ class: 'vbox' }).add(
        view({
            style: "f:26;clr:#585858;mt:30;",
            text: "伙食费剩余￥4000.00"
        }),
        view({
            style: "f:26;clr:#8d8d8d8d;mt:30;",
            text: "注：提现金额需预留1个月额度"
        }),
        part1.mybottom("提现", "./04.png"),

    ),

    view({ class: 'hbox cc' }).add(
        view({
            style: "f:68;clr:#000000;",
            text: "￥2500.00"
        })
    ),

    view({
        style: "f:28;clr:#000000;mt:20",
        text: "提现金额"
    })

).add(

);

fs.writeFileSync(__dirname + '\\yey-bjjf-tx.vue', pg1.code, 'utf8');