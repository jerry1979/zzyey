require('jbecool');
require('webmix');
const fs = require('fs');

var w = 339,
    h = 211;
var pr = (750 - w * 2) / 3;

var ss2 = style().add({
    '.item': {
        w: w,
        h: h,
        mt: pr,
        mr: pr,
        f: 36,
        clr: 'white'
    },
});
var pg1 = view({ class: 'hbox', style: { p: pr } }).add(
    view({ class: 'hbox c', style: 'w:750' }).add(
        view({
            class: 'item hbox mc cc',
            style: { bgimg: './01.png' },
            text: '伙食费'
        }),
        view({
            class: 'item hbox mc cc',
            style: { bgimg: './03.png' },
            text: '杂费'
        }),
        view({
            class: 'item hbox mc cc',
            style: { bgimg: './02.png' },
            text: '学费'
        })
    )
);

fs.writeFileSync(__dirname + '\\yey-jf.vue', ss2.code + pg1.code, 'utf8');