require('jbecool');
require('webmix');
const fs = require('fs');

var w = 339,
    h = 211;
var pr = (750 - w * 2) / 3;

const part1 = require('../../parts.js');


var pg1 = view({ class: 'vbox cc', style: { p: pr } }).add(

    image({
        src: './02.png',
        style: "w:123;h:123;"
    }),

    view({ class: 'vbox' }).add(
        view({ class: 'hbox cc', style: { w: 700 } }).add(
            view({
                style: "f:26;clr:#585858;",
                text: "本月"
            }),
            image({ src: "./07.png", style: 'ml:20;' }),
        ),

        part1.mybottom("立即缴费", "./04.png"),
    ),

    view({ class: 'hbox cc' }).add(
        view({
            style: "f:60;clr:#171717;",
            text: "￥"
        }),
        part1.myinput('200', "60", '#000000'),
    ),

    view({
        style: "f:28;clr:#171717;mt:20",
        text: "输入金额"
    })

);

fs.writeFileSync(__dirname + '\\yey-bjjf-jfzym.vue', pg1.code, 'utf8');