__unit = 'rpx';

exports.fixUrl = function (dirname, url) {
    var idx = dirname.lastIndexOf("\\pages\\") + 7;
    var r = J.us.replaceAll(dirname.substr(idx), "\\", "/");
    return "http://jerry.xtbeiyi.com/pages/" + r + "/" + url;
};

exports.mybottom = function (str, img) {
    return button({
        style: {
            'outline': '0 none !important',
            'border-radius': '30px',
            mt: '30',
            mb: '20',
            bgimg: img,
            border: 0,
            w: 573,
            h: 71,
            f: 32,
            clr: "#ffffff"
        },
        text: str
    })
};

exports.myinput = function (h, f, clr, str) {
    return input({
        style: {
            bdr: 'none',
            'outline': 'none',
            h: h,
            f: f,
            clr: clr
        },
        placeholder: str
    })
};

exports.mybgimg = function (w, h, img) {
    return view({
        style: {
            w: w,
            h: h,
            bgimg: img
        }
    })
};

exports.mytext = function (f, clr, str) {
    return view({
        style: {
            ml: 15,
            mr: 15,
            f: f,
            clr: clr
        },
        text: str
    })
};