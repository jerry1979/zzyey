require('jbecool');
require('webmix');
const fs = require('fs');

var w = 339, h = 211;
var pr = (750 - w * 2) / 3;

const part1 = require('../parts.js');

var ss2 = style().add({
    '.img':{
        w:613,
        mt:"5%",
        mb:'5%'
    },
    '.text':{
        mr:10,
        f:26,
        clr:"#171717"
    },
    '.flex':{
        'display': 'flex',
        'justify-content': 'space-between'
    }
});


var page1 = view({ class: 'vbox cc', style: {pt:50,w:750,h:712,bgimg: "./06.png"}}).add(
    view({class: ' hbox c',style:"w:613"}).add(
        view({class:'text',text:"姓名"}),
        part1.myinput("39","26","#171717","请输入姓名")
        
    ),

    
    part1.mybottom("确　定","./05.png"),

    view({
        class:'tr',
        style:'mt:5%;f:20;clr:#6e7ea2;w:613',
        text:"已有账号？"
    }),

    view({class: ' hbox',style:"w:613"}).add(
        image({
            src: './03.png',
            style:"w:33;h:33"
        }),
        view({style:'ml:10;f:20;clr:#171717',text:"我已阅读XX隐私保护规则"})
        
    ),

    image({
        class:'img',
        src: './01.png'
    }),


    view({class:'flex',style:"w:613"}).add(
        view({class:'text',text:"职务"}),
        image({
            src: './02.png',
            style:"w:24;h:16;mt:20;"
        })
    ),

    image({
        class:'img',
        src: './01.png'
    }),

    view({class: ' hbox',style:"w:613"}).add(
        view({class:'text',text:"电话号码"}),
        part1.myinput("39","26","#171717","请输入号码")
    ),

    image({
        class:'img',
        src: './01.png'
    }),
);

fs.writeFileSync(__dirname + '\\yey-wsgrxx.vue', ss2.code + page1.code, 'utf8');
