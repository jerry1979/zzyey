require('jbecool');
require('webmix');
const fs = require('fs');
const part1 = require('../../parts.js');

 var part2 = part1.mybgimg(750,38,"./04.png");
 part2._class = ["vbox cc"];




var page1 = view({class:" vbox cc "}).add(
    view({class:"vbox cc",style:"w:750;bgclr:#0f0"}).add(
        view({class:"hbox cc",style:{bgimg:"./01.png",w:703,h:104,'justify-content': 'space-between'}}).add(
            part1.mytext(36,"#fff0c6","应缴 ￥55000.00"),
            view({class:"hbox cc",style:"justify-content: flex-start;h:80"}).add(
                part1.mytext(26,"#fff","本月"),
                image({src:"./10.png",style:"mr:20"}),
            ),
        )
    ),

    image({src:"./09.png"}),
    view({class:"hbox cc",style:"justify-content:space-between;w:700;h:100;"}).add(
        view({class:"hbox cc"}).add(
            image({src:"./08.png"}),
            view({class:"vbox"}).add(
                part1.mytext(28,"#171717","119班"),
                part1.mytext(18,"#b2b2b2","共计金额: 10000.00元"),
            )
        ),
        view({class:"hbox cc"}).add(
            view({class:"tc",style:{bgimg:"./05.png",w:99,h:31,clr:"#fff",f:20,mr:10},text:"已缴40人"}),
            view({class:"tc",style:{bgimg:"./03.png",w:99,h:31,clr:"#fff",f:20,mr:10},text:"未缴3人"}),
        ),
    ),

    image({src:"./09.png"}),
    view({class:"hbox cc",style:"justify-content:space-between;w:700;h:100;"}).add(
        view({class:"hbox cc"}).add(
            image({src:"./07.png"}),
            view({class:"vbox"}).add(
                part1.mytext(28,"#171717","150班"),
                part1.mytext(18,"#b2b2b2","共计金额: 15000.00元"),
            )
        ),
        view({class:"hbox cc"}).add(
            view({class:"tc",style:{bgimg:"./05.png",w:99,h:31,clr:"#fff",f:20,mr:10},text:"已缴40人"}),
            view({class:"tc",style:{bgimg:"./03.png",w:99,h:31,clr:"#fff",f:20,mr:10},text:"未缴3人"}),
        ),
    ),

    image({src:"./09.png"}),
    view({class:"hbox cc",style:"justify-content:space-between;w:700;h:100;mt:30"}).add(
        view({class:"hbox cc"}).add(
            image({src:"./06.png"}),
            view({class:"vbox"}).add(
                part1.mytext(28,"#171717","164班"),
                part1.mytext(18,"#b2b2b2","共计金额: 15000.00元"),
            )
        ),
        view({class:"hbox cc"}).add(
            view({class:"tc",style:{bgimg:"./05.png",w:99,h:31,clr:"#fff",f:20,mr:10},text:"已缴40人"}),
            view({class:"tc",style:{bgimg:"./03.png",w:99,h:31,clr:"#fff",f:20,mr:10},text:"未缴3人"}),
        ),
    ),

    view({class:"hbox cc",style:{bgimg:"./04.png",w:717,h:167,'justify-content': 'space-around'}}).add(
        view({class:"vbox cc"}).add(
            part1.mytext(32,"#efb025","￥40000.00"),
            part1.mytext(24,"#3a3a3a","已缴"),
        ),
        view({class:"vbox cc"}).add(
            part1.mytext(32,"#efb025","￥15000.00"),
            part1.mytext(24,"#3a3a3a","未缴"),
        )
    ),

)

fs.writeFileSync(__dirname + '\\yey-lsckjfjl.vue', page1.code, 'utf8');