require('jbecool');
require('webmix');
const fs = require('fs');

var w = 339, h = 211;
var pr = (750 - w * 2) / 3;
const part1 = require('../../../parts.js');

var ss2 = style().add({
    '.item': {
        w: w,
        h: h,
        mt: pr,
        mr: pr
    },
});
var page1 = view({ class: 'hbox', style: { p: pr } }).add(
    view({
        class: 'item vbox mc cc',
        style: { bgimg: './01.jpg' }
    }).add(
        part1.mytext(36,"#ffffff","伙食费"),
        part1.mytext(36,"#ffffff","缴费情况"),
    ),

    view({
        class: 'item vbox mc cc',
        style: { bgimg: './02.jpg' }
    }).add(
        part1.mytext(36,"#ffffff","学　费"),
        part1.mytext(36,"#ffffff","缴费情况"),
    ),
);

fs.writeFileSync(__dirname + '\\yey-jf.vue',ss2.code + page1.code, 'utf8');
