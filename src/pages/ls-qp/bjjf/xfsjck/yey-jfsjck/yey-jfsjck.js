require('jbecool');
require('webmix');
const fs = require('fs');

var w = 339, h = 211;
var pr = (750 - w * 2) / 3;

const part1 = require('../../../parts.js');
var part2 = part1.mybgimg(716,270,"./03.png");
part2._class = ["vbox cc"];

var part3 = part1.mybgimg(168,108,"./01.png");
part3._class = ["vbox cc"];


var page1 = view({ class: 'vbox cc'}).add(
    
    part2.add(
        part1.mytext(50,"#171717","52"),
        view({class: 'hbox',style:"justify-content: space-around;w:716"}).add(
            part3.add(
                part1.mytext(50,"#ffffff","45"),
                part1.mytext(24,"#ffffff","应邀（人）"),
            ),
            view({ class: 'vbox cc'}).add(
                part1.mytext(50,"#171717","7"),
                part1.mytext(24,"#171717","未邀（人）"),
            )
        ),
        part1.mytext(24,"#171717","应邀（人）"),
    ),

    image({src:"./06.png",style:"w:708;h:1"}),

    view({ class: 'hbox',style:"justify-content:space-between;w:750;mb:10;mt:20"}).add(
        view({ class: 'hbox'}).add(
            image({src: './03.png',style:"w:82;h:82"}),
            view({ class: 'vbox',style:"justify-content: space-around"}).add(
                part1.mytext(28,"#171717","程子涵"),
                part1.mytext(18,"#585858","￥2500.00"),
            )
        ),
        view({ class: 'vbox cc',style:"justify-content: space-around"}).add(
            part1.mytext(18,"#acacac","10月4日 09:46"),
            part1.mybgimg(85,31,"./05.png").add(
                view({ class: 'hbox mc',style:"f:20;clr:#ffffff",text:"已缴"})
            ),
        )
    ),

    image({src:"./04.png",style:"w:750;h:18"}),

    view({class: 'hbox',style:"align-items: flex-end;justify-content: space-between ;w:716;mt:30;mb:15"}).add(
        part1.mytext(30,"#171717","本月"),
        part1.mytext(20,"#585858","共计金额:￥50380.00"),
    ),

);

fs.writeFileSync(__dirname + '\\yey-jfsjck.vue',page1.code, 'utf8');
