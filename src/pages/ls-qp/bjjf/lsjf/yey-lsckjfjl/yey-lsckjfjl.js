require('jbecool');
require('webmix');
const fs = require('fs');
const part1 = require('../../../parts.js');

 var part2 = part1.mybgimg(750,38,"./04.png");
 part2._class = ["vbox cc"];




var page1 = view({class:" vbox cc "}).add(
    view({class:"vbox cc",style:"w:750;bgclr:#0f0"}).add(
        view({class:"hbox cc",style:"w:710"}).add(
            image({src:"./03.png",style:"m:15"}),
            view({class:"tl"}).add(
                part1.mytext(30,"#ffffff","李樾抒  XXX老师"),
                part1.mytext(24,"#ffffff","（XXX班级）"),
            )
        ),
        view({class:"hbox cc",style:{bgimg:"./01.png",w:703,h:104,'justify-content': 'space-between'}}).add(
            part1.mytext(36,"#fff0c6","余额 ￥2500.00"),
            button({
                style:{
                    'outline':'0 none !important',
                    'border-radius':'15px',
                    bgimg: "./02.png",
                    mr:15,
                    w:152,
                    h:56,
                    f:30,
                    clr:"#ffffff"
                },
                text:"提现"
            })
        )
    ),
    image({src:"./09.png"}),
    view({class:"hbox cc",style:"justify-content:space-between;w:750;h:100;"}).add(
        view({class:"hbox cc"}).add(
            image({src:"./08.png"}),
            view({class:"vbox"}).add(
                part1.mytext(28,"#171717","建设银行充值"),
                part1.mytext(22,"#b2b2b2","10月4日09:46"),
            )
        ),
        part1.mytext(28,"#efb025","+3800.85")
    ),

    image({src:"./09.png"}),
    view({class:"hbox cc",style:"justify-content:space-between;w:750;h:100;"}).add(
        view({class:"hbox cc"}).add(
            image({src:"./07.png"}),
            view({class:"vbox"}).add(
                part1.mytext(28,"#171717","扣款"),
                part1.mytext(22,"#b2b2b2","10月4日09:46"),
            )
        ),
        part1.mytext(28,"#171717","-1851.85")
    ),

    image({src:"./05.png"}),

    view({class:"hbox cc",style:"justify-content: flex-start;w:750;h:80"}).add(
        part1.mytext(26,"#171717","2020年10月"),
        image({src:"./06.png"}),
    ),
    view({class:"hbox cc",style:{bgimg:"./04.png",w:717,h:167,'justify-content': 'space-around'}}).add(
        view({class:"vbox cc"}).add(
            part1.mytext(32,"#efb025","￥5000.00"),
            part1.mytext(24,"#3a3a3a","已缴"),
        ),
        view({class:"vbox cc"}).add(
            part1.mytext(32,"#efb025","￥2500.00"),
            part1.mytext(24,"#3a3a3a","扣款"),
        )
    ),

)

fs.writeFileSync(__dirname + '\\yey-lsckjfjl.vue',page1.code, 'utf8');