require('jbecool');
require('webmix');
const fs = require('fs');

var w = 339, h = 211;
var pr = (750 - w * 2) / 3;

const part1 = require('../../../parts.js');

var page1 = view({ class: 'vbox cc', style: { p: pr } }).add(
    view({ class: 'vbox cc', style: { w:750} }).add(
    image({ src: './04.png'}),

    view({style:"mt:200"}).add(
        part1.mybottom("立即缴费","./01.png"),
    ),

    view({class: 'hbox cc',style:"w:700;h:100"}).add(
        view({ style:"f:26;clr:#585858",text:"本月"}),
        image({src: './03.png'})
    ),
    image({src:"./02.png"}),

    
    view({class: 'hbox cc' ,style:"w:700;justify-content:flex-end;h:120"}).add(
        view({
            style:"f:60;clr:#171717;",
            text:"￥"
        }),
       input({style:"w:500;h:90;f:68;clr:#171717;bdr: 0;outline:none;"})
    ),

    view({
        style:"f:28;clr:#171717;mt:30;mb:30",
        text:"输入金额"
    })
    )
  
);

fs.writeFileSync(__dirname + '\\yey-bjjf-jfzym.vue',page1.code, 'utf8');
