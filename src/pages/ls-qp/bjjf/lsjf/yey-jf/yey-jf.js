require('jbecool');
require('webmix');
const fs = require('fs');

var w = 339, h = 211;
var pr = (750 - w * 2) / 3;

var ss2 = style().add({
    '.item': {
        w: w,
        h: h,
        mt: pr,
        mr: pr,
        f: 36,
        clr: 'white'
    },
});
var page1 = view({ class: 'hbox', style: { p: pr } }).add(
    view({
        class: 'item hbox mc cc',
        style: { bgimg: './01.png' },
        text: '工会费'
    }),
    view({
        class: 'item hbox mc cc',
        style: { bgimg: '02.png' },
        text: '生活费'
    }),
    view({
        class: 'item hbox mc cc',
        style: { bgimg: '03.png' },
        text: '教务费'
    }),
    view({
        class: 'item hbox mc cc',
        style: { bgimg: '04.png' },
        text: '学　费'
    }),
);

fs.writeFileSync(__dirname + '\\yey-jf.vue', ss2.code + page1.code, 'utf8');
