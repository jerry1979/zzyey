require('jbecool');
require('webmix');
const fs = require('fs');

var w = 339, h = 211;
var pr = (750 - w * 2) / 3;

const part1 = require('../../../parts.js');


var page1 = view({ class: 'vbox cc', style: { p: pr } }).add(

    image({
        src: './02.png',
        style:"w:123;h:123;"
    }),

    view({class: 'vbox'}).add(
        view({
            style:"f:26;clr:#585858;mt:30;",
            text:"生活费余额￥100.00"
        }),
        part1.mybottom("提　现","./01.png"),
    ),

    view({
        style:"f:68;clr:#171717;",
        text:"￥2500.00"
    }),

    view({
        style:"f:28;clr:#171717;mt:20",
        text:"提现金额"
    })
  
);

fs.writeFileSync(__dirname + '\\yey-bjjf-tx.vue', page1.code, 'utf8');
