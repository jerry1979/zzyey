require('jbecool');
require('webmix');
const fs = require('fs');

var w = 339, h = 211;
var pr = (750 - w * 2) / 3;

const part1 = require('../../../parts.js');

var page1 = view({ class: 'vbox cc'}).add(

    view({ class: 'hbox cc',style:"justify-content:space-between;w:750;h:80"}).add(
        part1.mytext(30,"#171717","李抒樾"),
        part1.mytext(30,"#929292","大班老师"),
    ),

    view({ class: 'hbox',style:"justify-content:space-between;w:750;mt:40;align-items:flex-end"}).add(
        button({
            style:{
                'outline':'0 none !important',
                'border-radius':'26px',
                bgimg:"./01.png",
                w:381,
                h:54,
                f:34,
                clr:"#ffffff"
            },
            text:"立即缴费"
        }),
        a({text:"查看缴费记录",style:"clr:#585858;f:22"})
    ),



    image({src:"./02.png"}),

    view({ class: 'hbox cc',style:"justify-content:space-between;w:750;h:80"}).add(
        part1.mytext(30,"#171717","余额:2500.00元"),
    ),

    image({src:"./03.png"}),

    view({ class: 'hbox cc',style:"justify-content:space-between;w:750;h:80"}).add(
        part1.mytext(30,"#171717","电话号码"),
        part1.mytext(30,"#171717","13786291110"),
    ),

    image({src:"./02.png"}),

    view({ class: 'hbox cc',style:"justify-content:space-between;w:750;h:80"}).add(
        part1.mytext(30,"#171717","缴费账号"),
        part1.mytext(30,"#171717","XXXXX"),
    ),

    image({src:"./02.png"})
    
  
);

fs.writeFileSync(__dirname + '\\yey-bjjf-jf.vue', page1.code, 'utf8');
