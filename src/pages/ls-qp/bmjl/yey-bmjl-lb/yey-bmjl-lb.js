require('jbecool');
require('webmix');
const fs = require('fs');

var w = 339, h = 211;
var pr = (750 - w * 2) / 3;

const part1 = require('../../parts.js');
var part2 = part1.mybgimg(716,204,"./05.png");
part2._class = ["vbox mc"];

var part3 = part1.mybgimg(168,108,"./02.png");
part3._class = ["vbox cc"];


var page1 = view({ class: 'vbox cc'}).add(
    
    part2.add(
        view({class: 'hbox',style:"justify-content: space-around;w:716"}).add(
            view({ class: 'vbox cc'}).add(
                part1.mytext(50,"#171717","45"),
                part1.mytext(24,"#171717","发起数"),
            ),
            part3.add(
                part1.mytext(50,"#ffffff","7"),
                part1.mytext(24,"#ffffff","参与数"),
            ),
        ),
    ),

    image({src:"./07.png",style:"w:660;h:2"}),

    view({ class: 'hbox',style:"justify-content:space-between;w:750;mb:10;mt:20"}).add(
        view({ class: 'hbox'}).add(
            image({src: './09.png',style:"w:64;h:64"}),
            view({ class: 'vbox',style:"justify-content: space-around"}).add(
                part1.mytext(28,"#171717","画画课程"),
                part1.mytext(24,"#8e8e8e","赵老师 2020.09.28 22:17"),
            )
        ),
        view({ class: 'vbox cc',style:"justify-content: space-around"}).add(
            part1.mybgimg(85,32,"./01.png").add(
                view({ class: 'hbox mc',style:"f:20;clr:#ffffff",text:"接龙"})
            ),
        )
    ),

    part1.mybgimg(750,18,"./03.png").add(
    ),

    view({class: 'hbox cc',style:"w:716;mt:30;mb:15"}).add(
        part1.mytext(30,"#171717","本月"),
        image({src:"./06.png",style:"w:24;h:16"})
    ),

);

fs.writeFileSync(__dirname + '\\yey-bmjl-lb.vue',  page1.code, 'utf8');
