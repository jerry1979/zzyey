require('jbecool');
require('webmix');
const fs = require('fs');

var w = 339, h = 211;
var pr = (750 - w * 2) / 3;

const part1 = require('../../parts.js');
var part2 = part1.mybgimg(750,249,"./03.png");
part2._class = ["hbox cc "];

var part3 = part1.mybgimg(750,249,"./03.png");
part3._class = ["hbox cc "];




var page1 = view({ class: 'vbox cc', style: { p: pr ,mt:"10%"} }).add(
    part2.add(
        image({src: './01.png',style:"w:95;h:117;ml:60;mr:13"}),
        view({class: 'vbox'}).add(
            part1.mytext(30,"#171717","发起报名接龙"),
            part1.mytext(24,"#666666","如:活动报名义工报名,周末亲子活动等"),
        )
    ),

    part3.add(
        image({src: './02.png',style:"w:108;h:90;ml:60"}),
        view({class: 'vbox'}).add(
            part1.mytext(30,"#171717","发布新鲜事"),
            part1.mytext(24,"#666666","如:义工活动,周末亲子活动等"),
        )
    ),

);

fs.writeFileSync(__dirname + '\\yey-fb-bmjl.vue',page1.code, 'utf8');
