require('jbecool');
require('webmix');
const fs = require('fs');
const part1 = require('../../parts.js');





var page1 = view({class:" vbox cc"}).add(

    view({class:"hbox cc",style:"justify-content:space-between;w:700;h:80"}).add(
        part1.mytext(30,"#171717","周末活动"),
        part1.mytext(30,"#8d8d8d","2020-09-25"),
    ),


    image({src:"./09.png"}),
    view({class:"hbox cc",style:"w:700;justify-content: space-between ;h:70"}).add(
        view({class:"hbox cc"}).add(
            image({src:"./08.png"}),
            part1.mytext(25,"#171717","凉凉子"),
        ),
        part1.mytext(20,"#979797","2020-09-28 18:00"),
    ),
    image({src:"./09.png"}),
    view({class:"hbox cc",style:"w:700;justify-content: space-between ;h:70"}).add(
        view({class:"hbox cc"}).add(
            image({src:"./07.png"}),
            part1.mytext(25,"#171717","凉凉子"),
        ),
        part1.mytext(20,"#979797","2020-09-28 18:00"),
    ),
    image({src:"./09.png"}),
    view({class:"hbox cc",style:"w:700;justify-content: space-between ;h:70;mt:10"}).add(
        view({class:"hbox cc"}).add(
            image({src:"./06.png"}),
            part1.mytext(25,"#171717","凉凉子"),
        ),
        part1.mytext(20,"#979797","2020-09-28 18:00"),
    ),


    part1.mytext(30,"#171717","—报名接龙名单—"),

    image({src:"./03.png",style:"mb:20"}),

    view({class:"hbox",style:"tl;w:700;h:80"}).add(
        part1.mytext(30,"#171717","已报名人数：35"),
        part1.mytext(30,"#171717","已查看人数40人"),
    ),

    image({src:"./03.png"}),

    view({class:"hbox",style:"tr;w:700;justify-content: flex-end ;h:70"}).add(
        view({class:"hbox cc"}).add(
            image({src:"./04.png"}),
            part1.mytext(22,"#171717","编辑"),
            
        ),
        view({class:"hbox cc"}).add(
            image({src:"./05.png"}),
            part1.mytext(22,"#171717","删除"),
            
        ),
    ),


    image({src:"./03.png"}),

    view({style:"tl;w:700"}).add(
        part1.mytext(30,"#171717","敬老院看望老人"),
        part1.mytext(20,"#8e8e8e","截止时间: 2020-09-28 23:59:00"),
        image({src:"./02.png",style:"m:15"}),
    ),

    image({src:"./01.png"}),
    
)

fs.writeFileSync(__dirname + '\\yey-bmjl-cyjl.vue',page1.code, 'utf8');