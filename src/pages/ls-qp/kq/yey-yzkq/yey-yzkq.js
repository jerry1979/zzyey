require('jbecool')
require('webmix')
const fs = require('fs')
const part1 = require('../../parts.js')

var part2 = part1.mybgimg(750, 38, './04.png')
part2._class = ['vbox cc']

var page1 = template().add(
    view({ class: ' vbox cc ' }).add(
        view({
            class: 'vbox cc mc',
            style: {
                bgimg: './16.png',
                w: 750,
                h: 449,
                'justify-content': 'space-around',
            },
        }).add(
            view({
                class: 'hbox cc',
                style: { bgimg: './01.png', w: 267, h: 65, pl: 40, mt: 60 },
            }).add(image({ src: './02.png' }), part1.mytext(32, '#ffffff', '4月')),

            view({
                class: 'hbox cc',
                style: 'justify-content: space-around;w:750;mb:60',
            }).add(
                part1.mytext(28, '#ffffff', '签到: 31天'),
                part1.mytext(28, '#ffffff', '总考勤率: 80%'),
                image({ src: './12.png' })
            )
        ),
        view({
            class: 'hbox cc',
            style: 'justify-content:space-between;w:750;h:80;mt:20',
        }).add(
            view({ class: 'hbox cc' }).add(
                image({ src: './09.png' }),
                part1.mytext(32, '#171717', '小班')
            ),
            part1.mytext(20, '#171717', '迟到 18 次')
        ),
        view({
            class: 'hbox cc',
            style: 'justify-content:space-between;w:750;h:80;mt:20',
        }).add(
            view({ class: 'hbox cc' }).add(
                image({ src: './07.png' }),
                part1.mytext(32, '#171717', '中班')
            ),
            part1.mytext(20, '#171717', '迟到 17 次')
        ),
        view({
            class: 'hbox cc',
            style: 'justify-content:space-between;w:750;h:80;mt:20',
        }).add(
            view({ class: 'hbox cc' }).add(
                image({ src: './06.png' }),
                part1.mytext(32, '#171717', '大班')
            ),
            part1.mytext(20, '#171717', '迟到 0 次')
        ),

        image({ src: '0./01.png' }),

        view({
            class: 'hbox cc',
            style: 'justify-content: flex-start;w:750;h:80',
        }).add(part1.mytext(26, '#171717', '2020年4月'), image({ src: './14.png' }))
    )
)

fs.writeFileSync(__dirname + '\\yey-yzkq.vue', page1.code, 'utf8')