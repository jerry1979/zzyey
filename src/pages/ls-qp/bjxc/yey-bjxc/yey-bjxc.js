require('jbecool');
require('webmix');
const fs = require('fs');

var w = 339, h = 211;
var pr = (750 - w * 2) / 3;
const part1 = require('../../parts.js');

var ss2 = style().add({
    '.item': {
        w: 333,
        h: 390,
        mt: pr,
        mr: pr
    },
});
var page1 = view({ class: 'hbox', style: { p: pr } }).add(
    view({
        class: 'item vbox mc cc',
        style: { bgimg: './02.png' ,'justify-content':'space-around'}
    }).add(
        image({ src:'./01.png',style: "w:198;h:214;"}),
        view({class:"hbox",style: "w:303;justify-content:space-between; align-items:flex-end;mt:5"}).add(
            part1.mytext(30,"#484848","照片"),
            part1.mybgimg(81,34,'./03.png').add(
                view({ class: 'hbox mc',style:"f:22;clr:#828283",text:"默认"})
            )
        )
    ),

    view({
        class: 'item vbox mc cc',
        style: { bgimg: './07.png' ,'justify-content':'space-around'}
    }).add(
        image({ src:'./06.png',style: "w:68;h:68;"}),
    ),
    
    view({
        class: 'item vbox mc cc',
        style: { bgimg: './05.png' ,'justify-content':'space-around'}
    }).add(
        image({ src:'./04.png',style: "w:163;h:111;mt:100"}),
        view({class:"hbox",style: "w:303;justify-content:space-between; align-items:flex-end;mt:5"}).add(
            part1.mytext(30,"#484848","视频"),
            part1.mybgimg(81,34,'./03.png').add(
                view({ class: 'hbox mc',style:"f:22;clr:#828283",text:"默认"})
            )
        )
    ),
    
);

fs.writeFileSync(__dirname + '\\yey-bjxc.vue',ss2.code + page1.code, 'utf8');
