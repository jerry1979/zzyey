require('jbecool');
require('webmix');
const fs = require('fs');

var w = 339, h = 211;
var pr = (750 - w * 2) / 3;
const part1 = require('../../parts.js');
var part2 = part1.mybottom("删除","./02.png");
part2._style.clr = '#f43737'
//console.log(part2);


var page1 = view({ class: 'vbox cc', style: { p: pr } }).add(
    view({class: 'hbox',style:"w:750;justify-content:space-between;align-items:flex-end;mb:30"}).add(
        part1.mytext(30,"#171717","上传到"),
        view({class: 'hbox cc'}).add(
            part1.mytext(24,"#8d8d8d","请选择相册"),
            image({src: "./07.png"})
        )
    ),

    

    part1.mybottom("确定发布","./06.png"),

    image({src:"./01.png",style:"mb:30"}),
    view({class: 'hbox',style:"w:700;mt:50;mb:50"}).add(
        view({class: 'vbox cc'}).add(
            image({src:"./04.png",style:"m:5"}),
            part1.mytext(30,"#171717","添加照片")
        ),
        view({class: 'vbox cc'}).add(
            image({src:"./05.png",style:"m:5;h:49"}),
            part1.mytext(30,"#171717","添加视频")
        ),
    ),

    image({src:"./02.png",style:"w:726;h:2;mt:20"}),

    view({class: 'hbox cc',style:"w:700"}).add(
        view({class: 'tr',style:{bgimg:"./09.png",w:158,h:165}}).add(
            image({src:"./08.png"}),
        ),
        
    ),

    image({src:"./01.png",style:"mb:30"}),
    
);

fs.writeFileSync(__dirname + '\\yey-bjxc-sczp.vue', page1.code, 'utf8');
