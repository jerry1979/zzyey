require('jbecool');
require('webmix');
const fs = require('fs');

var w = 339, h = 211;
var pr = (750 - w * 2) / 3;

const part1 = require('../../parts.js');


var page1 = view({ class: 'vbox cc'}).add(

    view({ class: 'vbox cc',style:{bgimg:"./11.png",w:750,h:357,'justify-content': 'flex-end'}}).add(
        button({
            style:{
                mb:100,
                f:32,
                border:0,
                clr:"#fff",
                'outline':'0 none !important',
                'border-radius':'30px',
                bgimg: "./01.png",
                w:376,
                h:66,
            },
            text: "上传照片"
        }),
        view({ class: 'hbox cc',style:"w:750;justify-content:space-between "}).add(
            view({ class: 'hbox cc'}).add(
                part1.mytext(22,"#d6d3cf","1张照片"),
                part1.mytext(22,"#d6d3cf","0个视频"),
            ),
            view({ class: 'hbox cc'}).add(
                image({src: './02.png'}),
                part1.mytext(22,"#fff","批量管理"),
                image({src: './03.png'}),
                part1.mytext(22,"#fff","编辑相册"),
            )
        )
    ),

    view({ class: 'vbox cc',style:" bgclr:#e6edf4;w:720;border-radius:5px"}).add(
        view({ class: 'hbox cc',style:"w:720;h:60"}).add(
            image({src: './09.png',style:"ml:15;mr:10"}),
            image({src: './08.png',style:"mr:10"}),
            image({src: './08.png',style:"mr:10"}),
            image({src: './08.png',style:"mr:10"}),
        ),
        view({ class: 'hbox cc',style:"justify-content:space-between;;w:720;h:60"}).add(
            view({ class: 'hbox cc'}).add(
                part1.mytext(22,"#506490","赵老师:"),
                part1.mytext(24,"#171717","点赞"),
            ),
            part1.mytext(22,"#6d6d6d","18:02"),
        ),
        view({style:"border:1px solid #fff;w:680"}),

        view({ class: 'hbox cc',style:"justify-content:space-between;;w:720;h:60"}).add(
            view({ class: 'hbox cc'}).add(
                part1.mytext(22,"#506490","李老师:"),
                part1.mytext(22,"#171717","好看耶！"),
            ),
            part1.mytext(22,"#6d6d6d","15:50"),
        ),
        view({style:"border:1px solid #fff;w:680"}),

    ),

    view({ class: 'vbox cc',style:{w:700}}).add(
        view({ class: 'hbox',style:"justify-content:space-between;w:720;h:90;mb:10;"}).add(
            view({ class: 'hbox cc'}).add(
                image({src: './04.png'}),
                part1.mytext(25,"#585858","2020.10.06 12:38"),
            )
        ),
        view({ class: 'hbox',style:"justify-content: flex-end;w:720;mt:15;mb:15"}).add(
            image({src: './06.png',style:"w:24;h:25"}),
            part1.mytext(22,"#666666","2"),
            image({src: './07.png',style:"w:24;h:25"}),
            part1.mytext(22,"#666666","10"),

        ),
        view({ class: 'hbox',style:"justify-content: flex-start;w:720"}).add(
            image({src: './05.png',style:"mr:10"}),
            image({src: './05.png',style:"mr:10"}),
            image({src: './05.png',style:"mr:10"}),
        ),

    ),

);

fs.writeFileSync(__dirname + '\\yey-bjxc-zp.vue', page1.code, 'utf8');
