require('jbecool');
require('webmix');
const fs = require('fs');
const part1 = require('../../parts.js');




var page1 = view({ class: " vbox cc" }).add(


    view({ class: "hbox cc", style: "justify-content:space-between;w:700;h:80" }).add(
        view({ class: "hbox cc" }).add(
            image({ src: "./07.png" }),
            part1.mytext(30, "#171717", "周珊珊")
        ),
        image({ src: "./08.png" }),
    ),

    part1.mybottom("确定", "./01.png"),

    image({ src: "./02.png" }),

    textarea({ style: "w:700;h:180;f:28;clr:#171717;p:10;mb:25", placeholder: "请输入评语" }),

    view({ class: "hbox cc", style: "justify-content:space-between;w:700;h:80" }).add(

        part1.mytext(30, "#171717", "老师评语"),

        image({ src: "./03.png" }),
    ),


    image({ src: "./02.png" }),

    view({ class: "hbox cc", style: "w:700;h:80" }).add(
        part1.mytext(30, "#171717", "学习表现"),
        image({ src: "./04.png" }),
        image({ src: "./04.png" }),
        image({ src: "./04.png" }),
        image({ src: "./04.png" }),
    ),

    view({ class: "hbox cc", style: "w:700;h:80" }).add(
        part1.mytext(30, "#171717", "生活表现"),
        image({ src: "./04.png" }),
        image({ src: "./04.png" }),
        image({ src: "./04.png" }),
    ),

    image({ src: "./02.png" }),

    view({ class: "tl", style: "w:700" }).add(
        part1.mytext(30, "#171717", "精彩瞬间"),
        image({ src: "./06.png", style: "m:15" }),
    ),
    image({ src: "./10.png" }),

    view({ class: "hbox cc", style: "justify-content:space-between;w:700;h:80" }).add(
        view({ class: "hbox cc" }).add(
            part1.mytext(30, "#171717", "2020年春季"),
            part1.mytext(30, "#171717", "第一周")
        ),
        image({ src: "./09.png" }),
    ),

    image({ src: "./10.png" }),


)

fs.writeFileSync(__dirname + '\\yey-czda.vue', page1.code, 'utf8');