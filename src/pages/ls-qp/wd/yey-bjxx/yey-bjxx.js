require('jbecool');
require('webmix');
const fs = require('fs');
const part1 = require('../../parts.js');

 var part2 = part1.mybgimg(750,38,"./04.png");
 part2._class = ["vbox cc"];




var page1 = view({class:" vbox cc"}).add(
    view({class:"hbox cc",style:{bgimg:"./01.png",w:750,h:318}}).add(
        image({src:"./11.png",style:"w:125;h:125;m:20"}),
        view({class:"hbox",style:"w:550;h:125;justify-content:space-between"}).add(
            view({class:"vbox mc"}).add(
                part1.mytext(28,"#ffffff","李抒樾 老师"),
                view({class:"hbox cc",style:""}).add(
                    image({src:"./06.png",style:"w:25;h:25"}),
                    part1.mytext(24,"#ffffff","切换班级"),
                )
            ),
            view({class:"vbox",style:"justify-content:space-between;align-items:flex-end"}).add(
                part1.mybgimg(121,35,"./08.png").add(
                    view({ class: 'hbox mc',style:"f:20;clr:#ffffff",text:"个人信息"})
                ),
                view({class:"hbox cc",style:""}).add(
                    image({src:"./09.png",style:"w:25;h:25"}),
                    part1.mytext(24,"#ffffff","邀请家长/老师"),
                )

            )
        )
    ),

    part1.mybottom("保存","0./01.png"),

    image({src:"./04.png",style:"w:750;h:18"}),

    view({class:"hbox cc",style:"justify-content:space-between;w:700;h:80"}).add(
        part1.mytext(28,"#171717","学段"),
        view({class:"hbox cc"}).add(
            part1.mytext(22,"#828495","大班，中班，小班"),
            image({src:"./10.png",style:"w:16;h:24"}),
        ),

        
    ),

    image({src:"./10.png",style:"w:715;h:1"}),

    view({class:"hbox cc",style:"justify-content:space-between;w:700;h:80"}).add(
        view({class:"hbox cc"}).add(
            part1.mytext(28,"#171717","关联学校"),
            part1.mytext(28,"#171717","未设置")
        ),
        image({src:"./10.png",style:"w:16;h:24"}),
    ),

    image({src:"./04.png",style:"w:750;h:18"}),
    

    view({class:"hbox cc",style:"justify-content:space-between;w:700;h:80"}).add(
        view({class:"hbox cc"}).add(
            part1.mytext(28,"#171717","联系方式")
        ),
        part1.mytext(22,"#828495","自动获取"),
    ),

    image({src:"./10.png",style:"w:715;h:1"}),

    view({class:"hbox cc",style:"justify-content:space-between;w:700;h:80"}).add(
        view({class:"hbox cc"}).add(
            part1.mytext(28,"#171717","班级名称"),
            part1.mytext(28,"#171717","花儿班")
        )
    ),

    part2.add(
        view({class:"hbox",style:"justify-content:space-between;w:700"}).add(
            view({class:"hbox cc"}).add(
                image({src:"./07.png",style:"w:27;h:27;ml:15"}),
                part1.mytext(22,"#535355","班级信息"),
            ),
            view({class:"hbox cc"}).add(
                image({src:"./05.png",style:"w:27;h:27"}),
                part1.mytext(22,"#535355","编辑"),
            )
        ),
    ),
    
)

fs.writeFileSync(__dirname + '\\yey-bjxx.vue', page1.code, 'utf8');