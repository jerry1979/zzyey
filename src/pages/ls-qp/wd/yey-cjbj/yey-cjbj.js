require('jbecool');
require('webmix');
const fs = require('fs');
const part1 = require('../../parts.js');

var page1 = view({class:" vbox cc"}).add(

    view({class:"hbox cc",style:"justify-content:space-between;w:700;h:80"}).add(
        part1.mytext(28,"#171717","班级码"),

        part1.mytext(22,"#a3a4b0","自动获取"),

        view({class:"hbox cc"}).add(
            part1.mytext(28,"#171717","5200508088039"),
            part1.mytext(22,"#4a8f7f","复制")
        )
    ),

    part1.mybottom("创建班级","./03.png"),

    image({src:"./02.png",style:"w:750;h:18"}),

    view({class:"hbox cc",style:"justify-content:space-between;w:700;h:80"}).add(
        part1.mytext(28,"#171717","学段"),
        view({class:"hbox cc"}).add(
            part1.mytext(22,"#828495","大班，中班，小班"),
            image({src:"./04.png",style:"w:16;h:24"}),
        )
    ),

    image({src:"./01.png",style:"w:715;h:1"}),

    view({class:"hbox cc",style:"justify-content:space-between;w:700;h:80"}).add(
        view({class:"hbox cc"}).add(
            part1.mytext(28,"#171717","关联学校"),
            part1.mytext(28,"#171717","未设置")
        ),
        image({src:"./04.png",style:"w:16;h:24"}),
    ),

    image({src:"./02.png",style:"w:750;h:18"}),


    view({class:"hbox cc",style:"justify-content:space-between;w:700;h:80"}).add(
        part1.mytext(28,"#171717","联系方式"),

        part1.mytext(22,"#a3a4b0","自动获取"),
    ),


    image({src:"./01.png",style:"w:715;h:1"}),
    

    view({class:"hbox cc",style:"justify-content:space-between;w:700;h:80"}).add(
        view({class:"hbox cc"}).add(
            part1.mytext(28,"#171717","班级名称"),
            part1.mytext(28,"#171717","花儿班")
        )
    ),

    image({src:"./01.png",style:"w:715;h:1"}),

    
)

fs.writeFileSync(__dirname + '\\yey-cjbj.vue', page1.code, 'utf8');