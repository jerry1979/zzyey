require('jbecool');
require('webmix');
const fs = require('fs');
const part1 = require('../../parts.js');




var page1 = view({class:" vbox cc"}).add(
    
    view({class:"hbox cc",style:"justify-content:space-between;w:700;h:95"}).add(
        view({class:"hbox cc"}).add(
            image({src:"./01.png",style:"m:10"})
        ),
        view({class:"hbox cc"}).add(
            part1.mytext(26,"#8d8d8d","点击上传头像"),
            image({src:"./04.png",style:"w:16;h:24"}),
        ),
    ),

    part1.mybottom("保存修改","./02.png"),

    part1.mybottom("指定自己为班主任","./05.png"),

    view({class:"hbox cc",style:"justify-content:space-between;w:700;h:95"}).add(
        view({class:"hbox cc"}).add(
            part1.mytext(30,"#171717","任教科目"),
        ),
        view({class:"hbox cc"}).add(
            part1.mytext(26,"#8d8d8d","请选择"),
            image({src:"./04.png",style:"w:16;h:24"}),
        ),
    ),

    image({src:"./06.png"}),

    view({class:"hbox cc",style:"justify-content:space-between;w:700;h:95"}).add(
        part1.mytext(30,"#171717","电话号码"),
        part1.mytext(30,"#171717","13788252561"),
    ),

    image({src:"./06.png"}),

    view({class:"hbox cc",style:"justify-content:space-between;w:700;h:95"}).add(
        part1.mytext(30,"#171717","个人姓名"),
        part1.mytext(30,"#171717","赵红英"),
    ),

    image({src:"./06.png"}),

    
)

fs.writeFileSync(__dirname + '\\yey-wd-lsgrxx.vue', page1.code, 'utf8');