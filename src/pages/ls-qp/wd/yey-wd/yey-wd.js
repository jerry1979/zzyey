require('jbecool');
require('webmix');
const fs = require('fs');
const part1 = require('../../parts.js');

 var part2 = part1.mybgimg(750,38,"04.png");
 part2._class = ["vbox cc"];




var page1 = view({class:" vbox cc"}).add(
    view({class:"hbox cc",style:{bgimg:"01.png",w:750,h:318}}).add(
        image({src:"11.png",style:"w:125;h:125;m:20"}),
        view({class:"hbox",style:"w:550;h:125;justify-content:space-between"}).add(
            view({class:"vbox mc"}).add(
                part1.mytext(28,"#ffffff","李抒樾 老师"),
                view({class:"hbox cc",style:""}).add(
                    image({src:"12.png",style:"w:25;h:25;ml:15"}),
                    part1.mytext(24,"#ffffff","切换班级"),
                )
            ),
            view({class:"vbox",style:"justify-content:space-between;align-items:flex-end"}).add(
                part1.mybgimg(121,35,"02.png").add(
                    view({ class: 'hbox mc',style:"f:20;clr:#ffffff",text:"个人信息"})
                ),
                view({class:"hbox cc",style:""}).add(
                    image({src:"09.png",style:"w:25;h:25"}),
                    part1.mytext(24,"#ffffff","邀请家长/老师"),
                )

            )
        )
    ),

    image({src:"08.png",style:"w:750;h:18"}),

    
    view({class:"hbox cc",style:"justify-content:space-between;w:700;h:80"}).add(
        view({class:"hbox cc"}).add(
            image({src:"07.png",style:"w:25;h:25;m:10"}),
            part1.mytext(32,"#171717","清除缓存")
        ),
        view({class:"hbox cc"}).add(
            part1.mytext(20,"#878787","13M"),
            image({src:"03.png",style:"w:16;h:24"}),
        ),
    ),


    image({src:"10.png",style:"w:715;h:1"}),

    view({class:"hbox cc",style:"justify-content:space-between;w:700;h:80"}).add(
        view({class:"hbox cc"}).add(
            image({src:"05.png",style:"w:25;h:25;m:10"}),
            part1.mytext(32,"#171717","评语模板")
        ),
        image({src:"03.png",style:"w:16;h:24"}),
    ),

    image({src:"08.png",style:"w:750;h:18"}),

    view({class:"hbox cc",style:"justify-content:space-between;w:700;h:80"}).add(
        view({class:"hbox cc"}).add(
            image({src:"06.png",style:"w:25;h:25;m:10"}),
            part1.mytext(32,"#171717","客服/帮助")
        ),
        image({src:"03.png",style:"w:16;h:24"}),
    ),


    image({src:"10.png",style:"w:715;h:1"}),

    view({class:"hbox cc",style:"justify-content:space-between;w:700;h:80"}).add(
        view({class:"hbox cc"}).add(
            image({src:"04.png",style:"w:25;h:25;m:10"}),
            part1.mytext(32,"#171717","班级管理")
        ),
        image({src:"03.png",style:"w:16;h:24"}),
    ),
    
)

fs.writeFileSync(__dirname + '\\yey-wd.vue', page1.code, 'utf8');