require('jbecool');
require('webmix');
const fs = require('fs');
const part1 = require('../../parts.js');




var page1 = view({class:" vbox cc" ,style:"w:750"}).add(
    textarea({style:"w:700;h:500;f:32;clr:#171717;p:10;", placeholder:"写评语"}),
    part1.mybottom("保存","./01.png"),
    view({class:"hbox",style:"w:700;justify-content: space-around"}).add(
        view({class:"",style:"bgclr:#eeeeee;c;m:5"}).add(
            part1.mytext(32,"#8c8c8d","爱学习"),
        ),
        view({class:"",style:"bgclr:#eeeeee;c;m:5"}).add(
            part1.mytext(32,"#8c8c8d","互助友爱好朋友"),
        ),
        view({class:"",style:"bgclr:#eeeeee;c;m:5"}).add(
            part1.mytext(32,"#8c8c8d","动手能力强有画画天赋"),
        ),
        view({class:"",style:"bgclr:#eeeeee;c;m:5"}).add(
            part1.mytext(32,"#8c8c8d","助人为乐老师的小帮手"),
        ),
    ),
    
)

fs.writeFileSync(__dirname + '\\yey-wd-cjxdpymb.vue',page1.code, 'utf8');