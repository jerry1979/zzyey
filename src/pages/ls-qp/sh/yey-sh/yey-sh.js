require('jbecool');
require('webmix');
const fs = require('fs');
const part1 = require('../../parts.js');




var page1 = view({class:" vbox cc"}).add(
    part1.mybgimg(750,249,"./02.png").add(
        view({style:{bgimg:"./01.png",w:29,h:29,mt:40,ml:30}}).add(
            view({ class: 'hbox mc',style:"f:22;clr:#ffffff",text:"2"})
        ),
        view({ class: 'hbox cc',style:"pt:20;pl:60;"}).add(
            image({src: './03.png',style:"w:95;h:75;"}),
            view({class: 'vbox'}).add(
                part1.mytext(30,"#171717","您有2条加入班级消息待审核"),
                part1.mytext(24,"#666666","点击审核加入班级"),
            )
        )
    ),
    part1.mybgimg(750,249,"./02.png").add(
        view({style:{bgimg:"./01.png",w:29,h:29,mt:40,ml:30}}).add(
            view({ class: 'hbox mc',style:"f:22;clr:#ffffff",text:"1"})
        ),
        view({ class: 'hbox cc',style:"pt:20;pl:60;"}).add(
            image({src: './05.png',style:"w:95;h:75;"}),
            view({class: 'vbox'}).add(
                part1.mytext(30,"#171717","您有1条退费消息待审核"),
                part1.mytext(24,"#666666","点击审核退费"),
            )
        )
    ),
    part1.mybgimg(750,249,"./02.png").add(
        view({style:{bgimg:"./01.png",w:29,h:29,mt:40,ml:30}}).add(
            view({ class: 'hbox mc',style:"f:22;clr:#ffffff",text:"3"})
        ),
        view({ class: 'hbox cc',style:"pt:20;pl:60;"}).add(
            image({src: './04.png',style:"w:95;h:75;"}),
            view({class: 'vbox'}).add(
                part1.mytext(30,"#171717","您有3条家长嘱托提醒"),
                part1.mytext(24,"#666666","新的嘱托提醒"),
            )
        )
    )
)

fs.writeFileSync(__dirname + '\\yey-sh.vue', page1.code, 'utf8');