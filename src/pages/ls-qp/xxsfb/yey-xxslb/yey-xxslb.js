require('jbecool');
require('webmix');
const fs = require('fs');

var w = 339, h = 211;
var pr = (750 - w * 2) / 3;

const part1 = require('../../parts.js');


var page1 = view({ class: 'vbox cc'}).add(
    part1.mybgimg(750,54,"./01.png").add(
        view({ class: 'hbox cc',style:"justify-content:space-between"}).add(
            view({ class: 'hbox'}).add(
                part1.mytext(30,"#171717","本月"),
                image({src: './07.png',style:"ml:10;mt:10;w:27;h:19"}),
            ),
            view({ class: 'hbox'}).add(
                part1.mytext(24,"#535355","共发布5个"),
                part1.mytext(24,"#535355","批量删除"),
            )
        )
    ),
    view({ class: '',style:"box-shadow:1px 1px 3px #909090;border-radius:15px;m:20"}).add(
        view({ class: 'hbox',style:"justify-content:space-between;w:750;mb:10"}).add(
            view({ class: 'hbox'}).add(
                image({src: './03.png',style:"w:82;h:82"}),
                view({ class: 'vbox'}).add(
                    part1.mytext(30,"#171717","和平公园游玩"),
                    part1.mytext(24,"#8d8d8d","9-19 10:36"),
                )
            ),
            view({ class: 'hbox'}).add(
                image({src: './02.png',style:"w:19;h:23"}),
                part1.mytext(18,"#535355","删除"),
            )
        ),
        
        
        view({ class: 'hbox tl',style:"justify-content: flex-end"}).add(
            image({src: './04.png',style:"w:32;h:33;"}),
            
            part1.mytext(22,"#535355","分享"),

            image({src: './05.png',style:"w:32;h:33;"}),

            part1.mytext(22,"#535355","10"),

        ),
        view({ class: 'hbox'}).add(
            image({src: './06.png',style:"w:139;h:139;m:10"}),
            image({src: './06.png',style:"w:139;h:139;m:10"}),
            image({src: './06.png',style:"w:139;h:139;m:10"}),
            image({src: './06.png',style:"w:139;h:139;m:10"}),
        ),
        part1.mytext(28,"#171717","和平公园游玩啦!! 大家都在这里等你哦~"),

    


    ),
    
  
);

fs.writeFileSync(__dirname + '\\yey-xxslb.vue',page1.code, 'utf8');
