require('jbecool');
require('webmix');
const fs = require('fs');

var w = 339, h = 211;
var pr = (750 - w * 2) / 3;

const part1 = require('../../parts.js');
var part2 = part1.mybgimg(715,203,"./01.png");
part2._class = ["vbox mc"];

var part3 = part1.mybgimg(168,108,"./02.png");
part3._class = ["vbox cc"];


var page1 = view({ class: 'vbox cc'}).add(
    
    part2.add(
        view({class: 'hbox',style:"justify-content: space-around;w:716"}).add(
            part3.add(
                part1.mytext(50,"#ffffff","3"),
                part1.mytext(24,"#ffffff","待审核"),
            ),
            view({ class: 'vbox cc'}).add(
                part1.mytext(50,"#171717","7"),
                part1.mytext(24,"#171717","已审核"),
            )
        ),
    ),

    image({src:"./06.png",style:"w:708;h:1"}),

    view({ class: 'hbox',style:"justify-content:space-between;w:750;mb:10;mt:20"}).add(
        view({ class: 'hbox'}).add(
            image({src: './09.png',style:"w:64;h:64"}),
            view({ class: 'vbox',style:"justify-content: space-around"}).add(
                part1.mytext(28,"#171717","程子涵请假"),
                part1.mytext(24,"#8e8e8e","感冒生病请假 2020.09.28 22:17"),
            )
        ),
        view({ class: 'vbox cc',style:"justify-content: space-around"}).add(
            part1.mybgimg(85,31,"./08.png").add(
                view({ class: 'hbox mc',style:"f:20;clr:#ffffff",text:"待审核"})
            ),
        )
    ),

    part1.mybgimg(750,48,"./04.png").add(
        view({ class: 'hbox',style:"justify-content:space-between;align-items:flex-end"}).add(

            part1.mytext(22,"#818181","3个待审批"),

            view({ class: 'hbox cc',style:"justify-content:flex-end;"}).add(
                image({src:"./06.png",style:"w:27;h:27"}),
                part1.mytext(24,"#171717","批量审批"),
            )
        )
    ),

    view({class: 'hbox cc',style:"w:716;mt:30;mb:15"}).add(
        part1.mytext(30,"#171717","本月"),
        image({src:"./05.png",style:"w:24;h:16"})
    ),

);

fs.writeFileSync(__dirname + '\\yey-qj-lb.vue',  page1.code, 'utf8');
