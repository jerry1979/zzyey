require('jbecool');
require('webmix');
const fs = require('fs');

var w = 339, h = 211;
var pr = (750 - w * 2) / 3;

const part1 = require('../../parts.js');
var part2 = part1.mybgimg(715,203,"./03.png");
part2._class = ["vbox mc"];

var part3 = part1.mybgimg(168,108,"./01.png");
part3._class = ["vbox cc"];


var page1 = view({ class: 'vbox cc'}).add(
    
    part2.add(
        view({class: 'hbox',style:"justify-content: space-around;w:716"}).add(
            part3.add(
                part1.mytext(50,"#ffffff","3"),
                part1.mytext(24,"#ffffff","待审核"),
            ),
            view({ class: 'vbox cc'}).add(
                part1.mytext(50,"#171717","7"),
                part1.mytext(24,"#171717","已审核"),
            ),
        ),
    ),

    view({ class: 'hbox',style:"justify-content:space-between;w:750;mb:10;mt:20"}).add(
        view({ class: 'hbox'}).add(
            image({src: './09.png',style:"w:64;h:64"}),
            view({ class: 'vbox',style:"justify-content: space-around"}).add(
                part1.mytext(30,"#171717","周佳佳申请加入班级"),
                part1.mytext(24,"#8e8e8e","2020.09.28 22:17"),
            )
        ),
        view({ class: 'vbox cc',style:"justify-content: space-around"}).add(
            part1.mybgimg(85,32,"./07.png").add(
                view({ class: 'hbox mc',style:"f:20;clr:#ffffff",text:"待审核"})
            ),
        )
    ),

    image({src:"./11.png"}),

    view({ class: 'hbox',style:"justify-content:space-between;w:750;mb:10;mt:20"}).add(
        view({ class: 'hbox'}).add(
            image({src: './09.png',style:"w:64;h:64"}),
            view({ class: 'vbox',style:"justify-content: space-around"}).add(
                part1.mytext(30,"#171717","周佳佳申请加入班级"),
                part1.mytext(24,"#8e8e8e","2020.09.28 22:17"),
            )
        ),
        view({ class: 'vbox cc',style:"justify-content: space-around"}).add(
            part1.mybgimg(85,32,"./07.png").add(
                view({ class: 'hbox mc',style:"f:20;clr:#ffffff",text:"待审核"})
            ),
        )
    ),

    part1.mybgimg(750,48,"./05.png").add(
        view({ class: 'hbox cc',style:"justify-content: space-between;h:48"}).add(
            part1.mytext(22,"#8e8e8e","3个待审批"),

        view({ class: 'hbox cc'}).add(
            image({src: './06.png'}),
            part1.mytext(24,"#171717","批量审批"),
        ),
        )
    ),

    view({class: 'hbox cc',style:"w:716;mt:30;mb:15"}).add(
        part1.mytext(24,"#171717","本月"),
        image({src:"./04.png"})
    ),

);

fs.writeFileSync(__dirname + '\\yey-jrbj-lb.vue', page1.code, 'utf8');
