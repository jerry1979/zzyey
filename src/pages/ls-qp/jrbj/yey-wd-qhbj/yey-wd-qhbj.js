require('jbecool');
require('webmix');
const fs = require('fs');
const part1 = require('../../parts.js');




var page1 = view({class:" vbox cc"}).add(

    view({style:{bgimg:"./06.png",w:750,h:36,tr}}).add(
        part1.mytext(22,"#858589","您已加入以下班级"),
    ),
    part1.mybottom("加入此班级","./05.png"),

    view({style:{bgimg:"./03.png",w:750,h:296}}).add(
        image({src:"./02.png",style:"m:50 10 0 680"}),
        view({ class: 'hbox',style:"pl:60;"}).add(
            image({src: './04.png'}),
            view({class: 'vbox'}).add(
                part1.mytext(30,"#171717","小班 班级名称"),
                part1.mytext(24,"#666666","包子 妈妈  梁梁子 爸爸"),
            )
        )
    ),

    view({style:{bgimg:"./03.png",w:750,h:296}}).add(
        image({src:"./01.png",style:"m:50 10 0 680"}),
        view({ class: 'hbox',style:"pl:60;"}).add(
            image({src: './04.png'}),
            view({class: 'vbox'}).add(
                part1.mytext(30,"#171717","大班 班级名称"),
                part1.mytext(24,"#666666","包子 妈妈  梁梁子 爸爸"),
            )
        )
    ),
)

fs.writeFileSync(__dirname + '\\yey-wd-qhbj.vue', page1.code, 'utf8');