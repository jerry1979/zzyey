require('jbecool');
require('webmix');
const fs = require('fs');

var w = 339, h = 211;
var pr = (750 - w * 2) / 3;

const part1 = require('../../parts.js');


var page1 = template().add(view({ class: 'vbox cc' }).add(

    swiper({ autoplay: true, circular: true, style: "h:291" }).add(
        swiperItem({}).add(
            view({}).add(
                image({ src: part1.fixUrl(__dirname, "01.png"), style: "w:703;h:291" })
            )
        )
    ),

    view({ class: 'hbox cc', style: { bgimg: part1.fixUrl(__dirname, "17.png"), w: 702, h: 65 } }).add(
        part1.mytext(22, "#7d93c4", "梁子妈妈:"),
        part1.mytext(24, "#171717", "去和平公园游玩啦？"),
    ),

    view({ class: 'vbox cc', style: { bgimg: part1.fixUrl(__dirname, "25.png"), w: 700 } }).add(
        view({ class: 'hbox', style: "justify-content:space-between;w:700;mb:10" }).add(
            view({ class: 'hbox' }).add(
                image({ src: part1.fixUrl(__dirname, "15.png"), style: "w:82;h:82" }),
                view({ class: 'vbox' }).add(
                    part1.mytext(30, "#171717", "和平公园游玩"),
                    part1.mytext(24, "#8d8d8d", "9-19 10:36"),
                )
            )
        ),
        view({ class: 'hbox', style: "justify-content: flex-end;w:700;mt:15;mb:15" }).add(
            image({ src: part1.fixUrl(__dirname, "19.png"), style: "w:32;h:32" }),
            part1.mytext(22, "#535355", "分享"),
            image({ src: part1.fixUrl(__dirname, "20.png"), style: "w:28;h:28" }),
            part1.mytext(22, "#535355", "10"),

        ),
        view({ class: 'hbox', style: "justify-content: flex-start;w:700" }).add(
            image({ src: part1.fixUrl(__dirname, "16.png"), style: "w:139;h:139;m:10" }),
            image({ src: part1.fixUrl(__dirname, "16.png"), style: "w:139;h:139;m:10" }),
            image({ src: part1.fixUrl(__dirname, "16.png"), style: "w:139;h:139;m:10" }),
            image({ src: part1.fixUrl(__dirname, "16.png"), style: "w:139;h:139;m:10" }),
        ),
        view({
            class: 'hbox', style: "w:670;f:28;clr:#171717", text: "和平公园游玩啦!! 大家都在这里等你哦~"
        })

    ),

    image({ src: part1.fixUrl(__dirname, "14.png"), style: "mb:30;w:702;h:2" }),

    view({ class: 'hbox cc', style: "justify-content:space-between;w:700" }).add(
        view({ style: "f:32;clr:#171717", text: "新鲜事" }),
        image({ src: part1.fixUrl(__dirname, "10.png"), style: "w:30;h:30" }),
    ),

    image({ src: part1.fixUrl(__dirname, "09.png"), style: "w:750;h:18" }),

    view({ class: 'hbox', style: { bgimg: part1.fixUrl(__dirname, "11.png"), w: 700, h: 157, mt: 10, mb: 10 } }).add(
        image({ src: part1.fixUrl(__dirname, "12.png"), style: "m:15;w:155;h:128;" }),

        view({ class: 'vbox', style: "w:500;justify-content:space-around" }).add(
            view({ class: 'hbox', style: " justify-content:space-between" }).add(
                part1.mytext(24, "#171717", "10月8日正常上班"),
                part1.mytext(24, "#343434", "10-05"),
            ),
            part1.mytext(20, "#584400", "蓝天幼儿园10月8日正常上班,请各位家长知悉蓝天幼儿园10月8日正常上班，请各位家长知悉...."),
        )
    ),

    view({ class: 'hbox cc', style: "justify-content:space-between;w:700;mt:10;mb:10" }).add(
        view({ class: 'hbox cc' }).add(
            image({
                src: part1.fixUrl(__dirname, "07.png"), style: "w:109;h:38;"
            }),
            part1.mytext(30, "#171717", "10月1号放假通知！"),
        ),
        view({ class: 'hbox' }).add(
            part1.mytext(24, "#343434", "更多"),
        )
    ),

    image({ src: part1.fixUrl(__dirname, "09.png"), style: "w:750;h:18;mt:20" }),
    view({ class: 'hbox cc', style: "w:700;justify-content: space-between" }).add(
        view({ class: 'vbox  cc' }).add(
            image({ src: part1.fixUrl(__dirname, "02.png"), style: "w:100;h:100;mt:20;mb:20" }),
            part1.mytext(22, "#343434", "请假")
        ),
        view({ class: 'vbox  cc' }).add(
            image({ src: part1.fixUrl(__dirname, "06-1.png"), style: "w:100;h:100;mt:20;mb:20" }),
            part1.mytext(22, "#343434", "成长档案")
        ),
        view({ class: 'vbox  cc' }).add(
            image({ src: part1.fixUrl(__dirname, "06.png"), style: "w:100;h:100;mt:20;mb:20" }),
            part1.mytext(22, "#343434", "审核")
        ),
        view({ class: 'vbox  cc' }).add(
            image({ src: part1.fixUrl(__dirname, "05.png"), style: "w:100;h:100;mt:20;mb:20" }),
            part1.mytext(22, "#343434", "班级相册")
        ),
        view({ class: 'vbox  cc' }).add(
            image({ src: part1.fixUrl(__dirname, "04.png"), style: "w:100;h:100;mt:20;mb:20" }),
            part1.mytext(22, "#343434", "活动接龙")
        ),
        view({ class: 'vbox  cc' }).add(
            image({ src: part1.fixUrl(__dirname, "03.png"), style: "w:100;h:100;mt:20;mb:20" }),
            part1.mytext(22, "#343434", "发布")
        ),
    ),

    image({ src: part1.fixUrl(__dirname, "09.png"), style: "w:750;h:18" }),

));

var js1 = script().add(function () {
    var def = {

    };
});

js1.text = js1.text.replace("var def = ", "export default");

fs.writeFileSync(__dirname + '\\sy.vue', page1.code + js1.code, 'utf8');
