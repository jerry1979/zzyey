require('jbecool');
require('webmix');
const fs = require('fs');

var w = 339, h = 211;
var pr = (750 - w * 2) / 3;

var ss2 = style().add({
    '.img':{
        w:125,
        h:125,
        mt:65,
        ml:40,
        mr:20
    }
    
});


var page1 = view({ class: 'vbox', style: { p: pr ,mt:"10%"} }).add(

    view({ class:"c",text:"邀请老师或家长加入班级",style:"f:28;clr:#171717;w:700"}),

    view({class: ' hbox c',style: { bgimg: './03.png' ,w:750,h:249}}).add(
        image({
            class:"img",
            src: './02.png'
        }),
        view({class:"vbox" ,style:"pt:85"}).add(
            view({text:"邀请家长加入班级",style:"f:30;clr:#171717"}),
            view({text:"生成家长专用的二维码",style:"f:24;clr:#666666"})
        )
    ),

    view({class: ' hbox c',style: { bgimg: './03.png' ,w:750,h:249}}).add(
        image({
            class:"img",
            src: './01.png'
        }),
        view({class:"vbox",style:"pt:85"}).add(
            view({text:"邀请老师加入班级",style:"f:30;clr:#171717"}),
            view({text:"生成老师专用的二维码",style:"f:24;clr:#666666"})
        )
    ),

);

fs.writeFileSync(__dirname + '\\yey-yq-ls-jz.vue', ss2.code + page1.code, 'utf8');
